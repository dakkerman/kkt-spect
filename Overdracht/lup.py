import numpy as np
import General_Functions as Gen
import Transport2D as Tr
import KKT_solver as Sol
import KKT_shape as Sh
import Minres
import time
from scipy.sparse import linalg as spla
from scipy import sparse
#########################################################################################################################
def cost_function(q, phi):
    difference = sigma_d.dot(phi) - Dphi
    J = 0.5*delta_x*np.dot(difference, difference) + .5*4*np.pi*cell_size*alpha*np.dot(q, q)
    return J
    
def minimum_check():
    scaling = 1e-3
    cost = cost_function(solution_linalg.get_q(orientation='vec'), solution_linalg.get_phi().get_vector())
    test_q = Sh.Grid2D(solution_linalg.get_q(orientation='vec'))
    test_phi = solution_linalg.get_phi().get_vector()
    print('Cost of solution: ', cost)
    for i in range(no_cells):
        pert_q = solution_linalg.get_q(orientation='vec').copy()
        pert_q[i] += scaling
        pert_q = Sh.Grid2D(pert_q)
        pert_phi = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, pert_q, print_output=False)[1]
        pert_cost = cost_function(pert_q.get_vector(), pert_phi.get_vector())
        cost_difference =  pert_cost - cost
        print('Perturbation: ', i, ' - cost difference: ', cost_difference)
        if cost_difference < 0:
            print('NOOOOOOOOOOOOOOOOOOO')
        pert_q = solution_linalg.get_q(orientation='vec').copy()
        pert_q[i] -= scaling
        pert_q = Sh.Grid2D(pert_q)
        pert_phi = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, pert_q, print_output=False)[1]
        pert_cost = cost_function(pert_q.get_vector(), pert_phi.get_vector())
        cost_difference =  pert_cost - cost
        print('Perturbation: ', i, ' - cost difference: ', cost_difference)
        if cost_difference < 0:
            print('NOOOOOOOOOOOOOOOOOOO')

# Preconditioning methods
def no_preco(vec):
    return vec


def physics_based_preconditioner(vec, y=None):
    if y is None:
        y = np.zeros(system_size)
    y[:no_cells] = Hconstant*(vec[:no_cells] - CT.dot(y[phisz:]))
    y[no_cells:phisz] = ainv.dot(vec[phisz:] - C.dot(y[:no_cells]))
    y[phisz:] = adjinv.dot(vec[no_cells:phisz] - KD.dot(y[no_cells:phisz]))
    y[:no_cells] = Hconstant*(vec[:no_cells] - CT.dot(y[phisz:]))
    y[no_cells:phisz] = ainv.dot(vec[phisz:] - C.dot(y[:no_cells]))
    return y


def c_product(vec):
    y = np.zeros(no_directions*no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(1+k)*no_cells] = -weight*np.pi*cell_size*vec
    return y


def ct_product(vec):
    y = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y -= weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    return y 
        

def cct_product(vec):
    temp = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        temp += weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    y = np.zeros(no_cells*no_directions)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(k+1)*no_cells] += weight*np.pi*cell_size*temp
    return y
        


def LU_P(vec):
    """
    This function uses the LU-decomposition as a preconditioner. All approximations should be made in The A33 block of A4.
    :param vec: The b   from P^-1*x = b
    :returns: x         from P^-1*x = b 
    """
    A1 = sparse.identity(system_size, format='lil')
    A1[phisz:,:no_cells] = Hconstant*C
    A1 = A1.tocsr()
    A2 = sparse.lil_matrix((system_size, system_size))
    A2[:no_cells, :no_cells] = sparse.identity(no_cells, format='lil')
    A2[no_cells:phisz, phisz:] = sparse.identity(no_cells*no_directions, format='lil')
    A2[phisz:, no_cells:phisz] = sparse.identity(no_cells*no_directions, format='lil')
    A2 = A2.tocsr()
    A3 = sparse.identity(system_size, format='lil')
    A3[phisz:, no_cells:phisz] = KD.dot(ainv)
    A3 = A3.tocsr()
    A4 = sparse.lil_matrix((system_size, system_size))
    A4[:no_cells, :no_cells] = 1/Hconstant * sparse.identity(no_cells, format='lil')
    A4[:no_cells, phisz:] = CT
    A4[no_cells:phisz, no_cells:phisz] = a
    A4[no_cells:phisz, phisz:] = -Hconstant * C.dot(CT)
    A4[phisz:,phisz:] = A23  # This is the part that may be approximated
    A4 = A4.tocsc()
    mat = A1.dot(A2.dot(A3.dot(A4)))
    y = sparse.linalg.spsolve(mat, vec)
    print(np.amax(y - LU_P_fast(vec)))
    #y = physics_based_preconditioner(vec, y)
    return y


def LU_P_fast(vec):
    """
    This function uses the LU-decomposition as a preconditioner. All approximations should be made in The A33 block of A4.
    :param vec: The b   from P^-1*x = b
    :returns: x         from P^-1*x = b 
    """
    y = np.zeros(system_size)
    rvec = np.zeros(system_size)
    rvec[:no_cells] = vec[:no_cells]
    rvec[no_cells:phisz] = -Hconstant*c_product(vec[:no_cells])
    rvec[no_cells:phisz] += vec[phisz:]
    #rvec[phisz:] = Hconstant*np.multiply(kd, spla.spsolve(a, c_product(vec[:no_cells])))
    rvec[phisz:] = Hconstant*sigma_d.dot(spla.spsolve(a, c_product(vec[:no_cells])))
    rvec[phisz:] += vec[no_cells:phisz]
    #rvec[phisz:] -= np.multiply(kd, spla.spsolve(a, vec[phisz:]))
    rvec[phisz:] -= sigma_d.dot(spla.spsolve(a, vec[phisz:]))
    y[:phisz] = rvec[:phisz]
    y[phisz:] = spla.spsolve(A23, rvec[phisz:])  #spla.spsolve(A23, rvec[phisz:])
    #y[phisz:] = rvec[phisz:] - adjinv.dot(KD.dot(ainv.dot(c_product(sparse.diags(nu/np.multiply(q_k, q_k)).dot(ct_product(rvec[phisz:])))))) 
    #y[phisz:] = A23inv.dot(rvec[phisz:])  #spla.spsolve(A23, rvec[phisz:])
    y[no_cells:phisz] = spla.spsolve(a, rvec[no_cells:phisz] + Hconstant*cct_product(y[phisz:]))
    y[:no_cells] = Hconstant*(rvec[:no_cells] - ct_product(y[phisz:]))
    return y


def A33(vec):
    temp = spla.spsolve(a, Hconstant*cct_product(vec))
    res = a.transpose().dot(vec) + K.dot(temp)
    return res
    

def A33_newton(vec):
    temp = spla.spsolve(a, c_product(Hstar.dot(ct_product(vec))))
    res = a.transpose().dot(vec) + K.dot(temp)
    return res
    

def A4(vec):
    print('A4')
    res = np.zeros(system_size)
    res[phisz:] = A33(vec[phisz:])
    res[no_cells:phisz] = a.dot(vec[no_cells:phisz]) - Hconstant*cct_product(vec[phisz:])
    res[:no_cells] = (1/Hconstant)*vec[:no_cells] + ct_product(vec[phisz:])
    return res


def A3(vec):
    print('A3')
    res = np.zeros(system_size)
    res[:phisz] = vec[:phisz]
    res[phisz:] = K.dot(spla.spsolve(a, vec[no_cells:phisz])) + vec[phisz:]
    return res


def A2(vec):
    print('A2')
    res = np.zeros(system_size)
    res[:no_cells] = vec[:no_cells]
    res[no_cells:phisz] = vec[phisz:]
    res[phisz:] = vec[no_cells:phisz]
    return res


def A1(vec):
    print('A1')
    res = np.zeros(system_size)
    res[:no_cells] = vec[:no_cells]
    res[no_cells:phisz] = vec[no_cells:phisz]
    res[phisz:] = Hconstant*c_product(vec[:no_cells]) + vec[phisz:]
    return res
    
def A123_inv_newton(vec):
    print('A3^-1 * A2^-1 * A1^-1')
    res = np.zeros(system_size)
    res[:no_cells] = vec[:no_cells]
    res[no_cells:phisz] -= c_product(Hstar.dot(vec[:no_cells]))
    res[no_cells:phisz] += vec[phisz:]
    res[phisz:] += K.dot(spla.spsolve(a, c_product(Hstar.dot(vec[:no_cells]))))
    res[phisz:] += vec[no_cells:phisz]
    res[phisz:] -= K.dot(spla.spsolve(a, vec[phisz:]))
    return res

def callback(vec):
    global callback_index
    print('Iteration number ' + str(callback_index) + ' \t Residual ' + '{:.2e}'.format(vec))
    residuals.append(vec)
    callback_index += 1


def newton_lup():
    global H_lup
    global H_lup_inv
    global callback_index
    preco = spla.LinearOperator((no_cells*no_directions, no_cells*no_directions), LU_P_new)
    A33_op = spla.LinearOperator((no_cells*no_directions, no_cells*no_directions), A33)
    phit_krylov = spla.gmres(A33_op, kkt_vec[no_cells:phisz], tol=tolerance, restart=gmres_restart, maxiter=maximum_iterations, M=preco, callback=callback)[0]
    x = np.zeros(system_size)
    q_k = -Hconstant*ct_product(phit_krylov)
    y3 = b2
    nu = nu_init
    for i in range(no_cells):
        if q_k[i] <= 0:
            q_k[i] = 1e-15
    phi0 = spla.spsolve(a, c_product(q_k))
    phit0 = spla.spsolve(a.transpose(), K.dot(phi0))
    phit_k = phit0
    x[:no_cells] = q
    converged = False
    i = 0
    while not converged:
        callback_index = 0
        sk = np.zeros(system_size)
        sk[:no_cells] += Hconstant*q_k
        sk[:no_cells] += ct_product(phit_k)
        sk[:no_cells] -= nu/q_k
        sk[phisz:] += A33(phit_k)
        sk[phisz:] -= K.dot(spla.spsolve(a, Hconstant*c_product(-nu/q_k)))
        sk[phisz:] -= y3
        H_lup_inv = Hconstant*np.ones(no_cells) + np.multiply(q_k, q_k)/nu
        A33_op = spla.LinearOperator((no_cells*no_directions, no_cells*no_directions), A33_newton)
        delta_phit_k = spla.gmres(A33_op, -sk[phisz:], tol=tolerance, restart=gmres_restart, maxiter=maximum_iterations, M=preco, callback=callback)[0]
        delta_q_k = np.multiply(H_lup_inv, -sk[:no_cells]) - np.multiply(H_lup_inv,ct_product(delta_phit_k))
        s = 1
        step_size_converged = False
        while not step_size_converged:
            if min(q_k + s*delta_q_k) <= 0:
                s *= 0.9
            else:
                step_size_converged = True
        phit_k += s*delta_phit_k
        q_k += s*delta_q_k
        relative_newton_norm = np.linalg.norm(delta_q_k)/np.linalg.norm(q)
        print('Newton step ', i, '\t nu = ', '{:.2e}'.format(nu), '\t relative norm = ' + '{:.2e}'.format(relative_newton_norm), '\t s = ' + '{:.2e}'.format(s))
        if relative_newton_norm  <= 1e-3 or s <= 1e-4:
            converged = True
            print('Newton converged on step  ', i, '\t with relative norm ', relative_newton_norm)
        nu *= 0.5
        if i % 3 == 0 or converged:
            x = np.zeros(system_size)
            x[:no_cells] = q_k
            write_x = Sh.KktVector(no_cells, cell_size, s_type, x, 2)
            input_object.write_source_to_file(write_x, 'normal_' + str(i), index)
        i += 1
        if i >= 90:
            print('Newton did not converge after 90 steps.')
            break
    return x, converged


def newton_lup():
    global Hstar
    global callback_index
    nu = nu_init
    x = np.ones(system_size)
    x[:no_cells] = solution_krylov[:no_cells]
    for i in range(no_cells):
        if x[i] <= 0:
            x[i] = 1e-15
    converged = False
    i = 0
    delta_xk = np.zeros(system_size)
    while not converged:
        #sk = mv_product(x)
        callback_index = 0
        sk = kkt_mat.dot(x)
        q_k = x[:no_cells]
        sk[:no_cells] -= nu/q_k
        sk[no_cells:phisz] -= detected_values
        hstar = Hconstant*np.zeros(no_cells) + np.multiply(q_k, q_k)/nu
        Hstar = sparse.diags(hstar, format='csr')
        A33_op = spla.LinearOperator((no_cells*no_directions, no_cells*no_directions), A33_newton)
        rhs = A123_inv_newton(-sk)
        delta_xk[phisz:] = spla.gmres(A33_op, rhs[phisz:], M=preco, restart=gmres_restart, tol=tolerance, maxiter=maximum_iterations, callback=callback)[0]
        delta_xk[no_cells:phisz] = spla.spsolve(a, rhs[no_cells:phisz] + c_product(Hstar.dot(ct_product(delta_xk[phisz:]))))
        delta_xk[:no_cells] = Hstar.dot(rhs[:no_cells] - ct_product(delta_xk[phisz:]))
        s = 1
        step_size_converged = False
        while not step_size_converged:
            if min(x + s*delta_xk) <= 0:
                s *= 0.9
            else:
                step_size_converged = True
        x += s*delta_xk
        relative_newton_norm = np.linalg.norm(delta_xk)/np.linalg.norm(x)
        print('Newton step ', i, '\t nu = ', '{:.2e}'.format(nu), '\t relative norm = ' + '{:.2e}'.format(relative_newton_norm), '\t s = ' + '{:.2e}'.format(s))
        if relative_newton_norm  <= 1e-3 or s <= 1e-4:
            converged = True
            print('Newton converged on step  ', i, '\t with relative norm ', relative_newton_norm)
        nu *= 0.5
        if i % 3 == 0 or converged:
            write_x = Sh.KktVector(no_cells, cell_size, s_type, delta_x*x, 2)
            input_object.write_source_to_file(write_x, 'lup' + str(i), index)
            print('\nThe difference between the input and the solution is ', calculate_difference(x[:no_cells]), '\n')
        i += 1
        if i >= 90:
            print('Newton did not converge after 90 steps.')
            break
    return x, converged


def newton():
    nu = nu_init
    x = np.ones(system_size)
    x[:no_cells] = solution_linalg.get_vector()[:no_cells]
    for i in range(no_cells):
        if x[i] <= 0:
            x[i] = 1e-15
    converged = False
    i = 0
    while not converged:
        #sk = mv_product(x)
        sk = kkt_mat.dot(x)
        q_k = x[:no_cells]
        sk[:no_cells] -= nu/q_k
        sk[no_cells:phisz] -= detected_values
        newt_mat = kkt_mat.copy()
        newt_mat[:no_cells, :no_cells] += sparse.diags(nu/np.multiply(q_k, q_k), 0, format='lil')
        delta_xk = spla.spsolve(newt_mat, -sk)
        s = 1
        step_size_converged = False
        while not step_size_converged:
            if min(x + s*delta_xk) <= 0:
                s *= 0.9
            else:
                step_size_converged = True
        x += s*delta_xk
        relative_newton_norm = np.linalg.norm(delta_xk)/np.linalg.norm(x)
        print('Newton step ', i, '\t nu = ', '{:.2e}'.format(nu), '\t relative norm = ' + '{:.2e}'.format(relative_newton_norm), '\t s = ' + '{:.2e}'.format(s))
        if relative_newton_norm  <= 1e-3 or s <= 1e-4:
            converged = True
            print('Newton converged on step  ', i, '\t with relative norm ', relative_newton_norm)
        nu *= 0.5
        if i % 3 == 0 or converged:
            write_x = Sh.KktVector(no_cells, cell_size, s_type, delta_x*x, 2)
            input_object.write_source_to_file(write_x, 'normal' + str(i), index)
        i += 1
        if i >= 90:
            print('Newton did not converge after 90 steps.')
            break
    return x, converged


def calculate_difference(q_in):
    src_len = np.linalg.norm(src.get_vector())
    dif_len = np.linalg.norm(src.get_vector() - q_in)
    return dif_len/src_len
    

def newton_gmres():
    global q_k
    global nu
    print('Initiating Newton process')
    nu = nu_init
    x = np.ones(system_size)
    #x = np.zeros(system_size)
    x[:no_cells] = solution_minres.get_vector()[:no_cells]
    #x = solution_minres
    for i in range(no_cells):
        if x[i] <= 0:
            x[i] = 1e-15
    #x[no_cells:phisz] = phi_real.get_vector()
    converged = False
    i = 0
    tolerance = 1e-5
    while not converged:
        sk = mv_product(x)
        q_k = x[:no_cells]
        sk[:no_cells] -= nu/q_k
        sk[no_cells:phisz] -= detection
        #linop_A = spla.LinearOperator((system_size, system_size), mv_product_newton)
        linop = spla.LinearOperator((system_size, system_size), preconditioner_type)
        newt_mat = kkt_mat.copy()
        newt_mat[:no_cells, :no_cells] += sparse.diags(nu/np.multiply(q_k, q_k), 0, format='lil')
        delta_x = spla.gmres(newt_mat, -sk, restart=gmres_restart, M=linop, maxiter=maximum_iterations, tol=tolerance, callback=callback)[0]
        #delta_x = spla.gmres(newt_mat, -sk, maxiter=system_size, tol=1e-5, restart=system_size, callback=callback)[0]
        print('Iteration number ', callback_index[0])
        s = 1
        step_size_converged = False
        """ while not step_size_converged:
            for j in range(no_cells):
                if x[j] + s*delta_x[j] <= 0:
                    s *= 0.9
                    break
                step_size_converged = True"""
        while not step_size_converged:
            step_size_converged = True
            for j in range(no_cells):
                if x[j] + s*delta_x[j] <= 0:
                    s *= 0.9
                    step_size_converged = False
        x += s*delta_x
        relative_newton_norm = np.linalg.norm(delta_x)/np.linalg.norm(x)
        print('Newton step ', i, '\t nu = ', '{:.2e}'.format(nu), '\t relative norm = ' + '{:.2e}'.format(relative_newton_norm), '\t s = ' + '{:.2e}'.format(s))
        if relative_newton_norm  <= 1e-3:
            converged = True
        nu *= 0.5
        if i % 3 == 0 or converged:
            write_x = Sh.KktVector(no_cells, cell_size, s_type, x, 2)
            input_object.write_source_to_file(write_x, 'gmres_' + str(i), index)
        i += 1
        if i >= 90:
            print('Newton did not converge after 90 steps')
            break
    return x

########################################################################################################################
# Initialization of user input constants
N = 8  # Number of cells in one x-direction
L = 1  # Length of the system
alpha = 1E-4  # Regularization constant
absorption_xsec = 0.048  #.048 cm^-1 - For 140 keV gammas
scatter_xsec = 0.172  #.172 cm^-1 - For 140 keV gammas
s_type = 8   # Measure for the amount of direction discretizations. Currently choose between S2 (1) and S4 (3)
preconditioner_type = LU_P_new
approximate = False
no_dimensions = 2
nu_init = 1e-8
detector_type = 0
tolerance = 1e-8
maximum_iterations = 500
gmres_restart = maximum_iterations

# Initialization of the computed system constants
no_cells = N**2  # Total number of cells
delta_x = L/N  # Cell length
cell_size = delta_x**2  # Cell surface area
no_directions = Sh.get_number_directions(no_dimensions, s_type)
phisz = (1+no_directions)*no_cells
direction_vectors = Sh.initialize_directions(no_dimensions, s_type)
input_object = Gen.KktCalculations(N, L, s_type, alpha, absorption_xsec, scatter_xsec)
absorption_xsec = Sh.Grid2D(absorption_xsec*np.ones(N**2))  # Place the input x-section onto a grid
scatter_xsec = Sh.Grid2D(scatter_xsec*np.ones(N**2))  # Place the input x-section onto a grid
src = Sh.Grid2D.initialize_center(N)  # Source is a square in the center
sigma_d = Sh.Flux2D.initialize_collimated_detector_fout(N, no_dimensions, s_type)
Hconstant = 1/(4*np.pi*cell_size*alpha)
#sigma_d = Sh.Flux2D.initialize_current_detector(N, no_dimensions, s_type)
K = delta_x*sigma_d.transpose().dot(sigma_d)
residuals = []
callback_index = 0


#######################################################################################################################
# Determine the forward transport matrix and calculate the flux from it
a, phi_real = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, src)
P = a.transpose() + (1/alpha)*K
#rl_det = Sh.Flux2D.initialize_collimated_detector_fout(N, no_dimensions, s_type)
#detected_values = Sh.Flux2D(K.dot(phi_real.get_vector()), s_type)
Dphi = sigma_d.dot(phi_real.get_vector())
detected_values = K.dot(phi_real.get_vector())
#detected_values2 = Sh.Flux2D(rl_det.dot(phi_real.get_vector()), s_type)

# Calculate the detected flux/current
"""print(detected_values.get_vector())
with open('sigmadtest.txt', 'w') as f:
    Gen.write_matrix(sigma_d.toarray(), f, approximate=False) 
"""
# Construct the KKT-matrix and -vector
kkt_mat, kkt_vec, system_size = Sol.create_kkt_system(2, s_type, delta_x, alpha, a, N, K, detected_values)
b2 = kkt_vec[no_cells:phisz]
print('The system has a total size of ' + str(system_size) + ' cells')


#######################################################################################################################
# Produce the linear solution for comparison
index = Gen.get_plot_number()

#Gen.write_matrix_to_textfile(kkt_mat.toarray(), 'kktmat') #Gen.write_matrix_to_textfile(kkt_vec, 'kktvec')
print('The elapsed time is: ' + str(time.process_time()))
#solution_linalg = Sh.KktVector(no_cells, cell_size, s_type, spla.spsolve(kkt_mat, kkt_vec), 2)
#solution_linalg2 = Sh.KktVector(no_cells, cell_size, s_type, spla.spsolve(kkt_mat2, kkt_vec2), 2)


#input_object.write_matrix_to_file(src.get_matrix(), 'source', index)

"""
print(.25*A1(A2(A3(A4(solution_linalg.get_vector()))))-kkt_mat.dot(solution_linalg.get_vector()))
print('\n-kkt_mat.dot(solution)-\n')
print(kkt_mat.dot(solution_linalg.get_vector()))
print('\n-kkt_vec-\n')
print(kkt_vec)
test_vec = np.zeros(system_size)
test_vec[phisz:] = kkt_vec[no_cells:phisz]
gmres_mat = spla.LinearOperator((system_size, system_size), A4)
solution_gmres = spla.gmres(gmres_mat, test_vec, tol=tolerance, restart=gmres_restart, maxiter=maximum_iterations, M=None, callback=callback)[0]
print(solution_linalg.get_vector())
print(solution_gmres)
"""

#######################################################################################################################
# Iterative system

preco = spla.LinearOperator((no_cells*no_directions, no_cells*no_directions), preconditioner_type)
A33_op = spla.LinearOperator((no_cells*no_directions, no_cells*no_directions), A33)
phit_krylov = spla.gmres(A33_op, b2, tol=tolerance, restart=gmres_restart, maxiter=maximum_iterations, M=preco, callback=callback)[0]
phi_krylov = spla.spsolve(a, Hconstant*cct_product(phit_krylov))
q_krylov = -Hconstant*ct_product(phit_krylov)
solution = np.zeros(system_size)
solution[:no_cells] = q_krylov
write_x = Sh.KktVector(no_cells, cell_size, s_type, solution, 2)
input_object.write_source_to_file(write_x, 'normal', index)

print('The elapsed time is: ' + str(time.process_time()))

solution_krylov = np.zeros(system_size)
solution_krylov[:no_cells] = q_krylov
solution_krylov[no_cells:phisz] = phi_krylov
solution_krylov[phisz:] = phit_krylov

newton_lup2()


solution_linalg = Sh.KktVector(no_cells, cell_size, s_type, spla.spsolve(kkt_mat, kkt_vec), 2)
newton()

print('End of program')
print('The elapsed time is: ' + str(time.process_time()))
exit()
#minimum_check()
solution_newton, converged = newton()
print('Newton solution converged = ', converged)
print('The elapsed time is: ' + str(time.process_time()))
#######################################################################################################################

#solution_minres, errors = Minres.solve_minres_preconditioned(kkt_mat, kkt_vec, no_cells, s_type, a, adj, alpha, cell_size, sigma_d.get_vector(), preconditioner_type=preconditioner_type, approximate=approximate, accuracy=1e-6, issparse=issparse)
print('\n')
