import numpy as np
import General_Functions as Gen
import Transport2D as Tr
import KKT_solver as Sol
import KKT_shape as Sh
import Minres
import time
from scipy.sparse import linalg as spla
from scipy import sparse
from random import uniform
#########################################################################################################################
def cost_function(q, phi):
    difference = sigma_d.dot(phi) - Dphi
    J = 0.5*delta_x*np.dot(difference, difference) + .5*4*np.pi*cell_size*alpha*np.dot(q, q)
    return J
    
def minimum_check():
    global Dphi
    Dphi = sigma_d.dot(phi_real.get_vector())
    scaling = 1e-3
    cost = cost_function(solution_linalg.get_q(orientation='vec'), solution_linalg.get_phi().get_vector())
    test_q = Sh.Grid2D(solution_linalg.get_q(orientation='vec'))
    test_phi = solution_linalg.get_phi().get_vector()
    print('Cost of solution: ', cost)
    for i in range(no_cells):
        pert_q = solution_linalg.get_q(orientation='vec').copy()
        pert_q[i] += scaling
        pert_q = Sh.Grid2D(pert_q)
        pert_phi = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, pert_q, print_output=False)[1]
        pert_cost = cost_function(pert_q.get_vector(), pert_phi.get_vector())
        cost_difference =  pert_cost - cost
        print('Perturbation: ', i, ' - cost difference: ', cost_difference)
        if cost_difference < 0:
            print('Not a minimum')
        pert_q = solution_linalg.get_q(orientation='vec').copy()
        pert_q[i] -= scaling
        pert_q = Sh.Grid2D(pert_q)
        pert_phi = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, pert_q, print_output=False)[1]
        pert_cost = cost_function(pert_q.get_vector(), pert_phi.get_vector())
        cost_difference =  pert_cost - cost
        print('Perturbation: ', i, ' - cost difference: ', cost_difference)
        if cost_difference < 0:
            print('Not a minimum')


def calculate_difference(q_in):
    src_len = np.linalg.norm(src.get_vector())
    dif_len = np.linalg.norm(src.get_vector() - q_in)
    return dif_len/src_len
    

# Preconditioning methods
def physics_based_newton(vec):
    res = np.zeros(system_size)
    h = 4*np.pi*cell_size*alpha*np.ones(no_cells) + nu/np.multiply(q_k, q_k)
    hinv = sparse.diags(1/h)
    res[:no_cells] = hinv.dot(vec[:no_cells] - ct_product(vec[phisz:]))
    res[no_cells:phisz] = spla.spsolve(a, vec[phisz:] - c_product(res[:no_cells]))
    res[phisz:] = spla.spsolve(a.transpose(), vec[no_cells:phisz] - K.dot(res[no_cells:phisz]))
    return res

def physics_based(vec):
    res = np.zeros(system_size)
    res[phisz:] = spla.spsolve(a.transpose(), vec[no_cells:phisz] - K.dot(res[no_cells:phisz]))
    res[:no_cells] = Hconstant*(vec[:no_cells] - ct_product(res[phisz:]))
    res[no_cells:phisz] = spla.spsolve(a, vec[phisz:] - c_product(res[:no_cells]))
    return res

def c_product(vec):
    y = np.zeros(no_directions*no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(1+k)*no_cells] = -weight*np.pi*cell_size*vec
    return y


def ct_product(vec):
    y = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y -= weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    return y 
        

def cct_product(vec):
    temp = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        temp += weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    y = np.zeros(no_cells*no_directions)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(k+1)*no_cells] += weight*np.pi*cell_size*temp
    return y
        


def callback(vec):
    global callback_index
    #print('Iteration number ' + str(callback_index) + ' \t Residual ' + '{:.2e}'.format(vec))
    residuals.append(vec)
    callback_index += 1



def newton():
    nu = nu_init
    x = np.ones(system_size)
    converged = False
    i = 0
    while not converged:
        #sk = mv_product(x)
        sk = kkt_mat.dot(x)
        q_k = x[:no_cells]
        sk[:no_cells] -= nu/q_k
        sk[no_cells:phisz] -= detected_values
        newt_mat = kkt_mat.copy()
        newt_mat[:no_cells, :no_cells] += sparse.diags(nu/np.multiply(q_k, q_k), 0, format='lil')
        delta_xk = spla.spsolve(newt_mat, -sk)
        s = 1
        step_size_converged = False
        while not step_size_converged:
            if min(x + s*delta_xk) <= 0:
                s *= 0.9
            else:
                step_size_converged = True
        x += s*delta_xk
        relative_newton_norm = np.linalg.norm(delta_xk)/np.linalg.norm(x)
        print('Newton step ', i, '\t nu = ', '{:.2e}'.format(nu), '\t relative norm = ' + '{:.2e}'.format(relative_newton_norm), '\t s = ' + '{:.2e}'.format(s))
        if relative_newton_norm  <= 1e-2 or s <= 1e-4:
            converged = True
            print('Newton converged on step  ', i, '\t with relative norm ', relative_newton_norm)
        nu *= 0.5
        #if i % 3 == 0 or converged:
        if i % 10 == 0 or converged:
            write_x = Sh.KktVector(no_cells, cell_size, s_type, x, 2)
            input_object.write_source_to_file(write_x, 'linalg' + str(i), index)
        if i == 10:
            break
        i += 1
        if i >= 90:
            print('Newton did not converge after 90 steps.')
            break
    return x, converged

def newton_gmres():
    global nu
    global q_k
    global callback_index
    nu = nu_init
    x = np.ones(system_size)
    converged = False
    i = 0
    while not converged:
        callback_index = 0
        sk = kkt_product(x)
        q_k = x[:no_cells]
        sk[:no_cells] -= nu/q_k
        sk[no_cells:phisz] -= detected_values
        newt_mat = kkt_mat.copy()
        newt_mat[:no_cells, :no_cells] += sparse.diags(nu/np.multiply(q_k, q_k), 0, format='lil')
        P = spla.LinearOperator((system_size, system_size), physics_based_newton)
        delta_xk = spla.gmres(newt_mat, -sk, tol=tolerance, restart=gmres_restart, maxiter=maximum_iterations, M=P, callback=callback)[0]
        residual = np.linalg.norm(newt_mat.dot(delta_xk)+sk)/np.linalg.norm(sk)
        s = 1
        step_size_converged = False
        while not step_size_converged:
            if min(x + s*delta_xk) <= 0:
                s *= 0.9
            else:
                step_size_converged = True
        x += s*delta_xk
        relative_newton_norm = np.linalg.norm(delta_xk)/np.linalg.norm(x)
        print('Newton step ', i, '\t nu = ', '{:.2e}'.format(nu), '\t residual = ' + '{:.2e}'.format(residual) + '\t #iterations ' + str(callback_index) + '\t relative norm = ' + '{:.2e}'.format(relative_newton_norm), '\t s = ' + '{:.2e}'.format(s))
        if relative_newton_norm  <= 1e-2 or s <= 1e-4:
            converged = True
            print('Newton converged on step  ', i, '\t with relative norm ', relative_newton_norm)
        nu *= 0.5
        if i % 3 == 0 or converged:
            write_x = Sh.KktVector(no_cells, cell_size, s_type, x, 2)
            input_object.write_source_to_file(write_x, 'gmres' + str(i), index)
        i += 1
        if i >= 20:
            print('Newton did not converge after 20 steps.')
            break
    return x, converged



########################################################################################################################
# Initialization of user input constants
no_dimensions = 2
N = 32  # Number of cells in one x-direction
s_type = 8   # Measure for the amount of direction discretizations. Currently choose between S2 (1) and S4 (3)
sigma_d = Sh.Flux2D.initialize_collimated_detector_fout(N, no_dimensions, s_type)
L = .5  # Length of the system
alpha = 1E-6  # Regularization constant
total_xsec = 15.8/(4*np.pi)
absorption_xsec = 0.85 * total_xsec  #0.048  #.048 cm^-1 - For 140 keV gammas
scatter_xsec = total_xsec-absorption_xsec  #0.172  #.172 cm^-1 - For 140 keV gammas
preconditioner_type = physics_based
approximate = False
nu_init = 1e-8
detector_type = 0
tolerance = 1e-6
maximum_iterations = 500
gmres_restart = maximum_iterations

# Initialization of the computed system constants
no_cells = N**2  # Total number of cells
delta_x = L/N  # Cell length
cell_size = delta_x**2  # Cell surface area
no_directions = Sh.get_number_directions(no_dimensions, s_type)
phisz = (1+no_directions)*no_cells
direction_vectors = Sh.initialize_directions(no_dimensions, s_type)
input_object = Gen.KktCalculations(N, L, s_type, alpha, absorption_xsec, scatter_xsec)
absorption_xsec = Sh.Grid2D(absorption_xsec*np.ones(N**2))  # Place the input x-section onto a grid
scatter_xsec = Sh.Grid2D(scatter_xsec*np.ones(N**2))  # Place the input x-section onto a grid
src = Sh.Grid2D.initialize_cactus(N)  # Source is a square in the center

Hconstant = 1/(4*np.pi*cell_size*alpha)
K = delta_x*sigma_d.transpose().dot(sigma_d)
residuals = []
callback_index = 0
index = Gen.get_plot_number()

#######################################################################################################################
# Determine the forward transport matrix and calculate the flux from it
a, phi_real = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, src)
phi_orig = phi_real.get_vector()
noise_size = 1e-2 

for i in range(no_cells*no_directions):
    noise = 1 + uniform(-noise_size, noise_size)
    phi_orig[i] *= noise
phi_real = Sh.Flux2D(phi_orig, s_type)
    

detected_values = K.dot(phi_real.get_vector())

# Construct the KKT-matrix and -vector
kkt_mat, kkt_vec, system_size = Sol.create_kkt_system(2, s_type, delta_x, alpha, a, N, K, detected_values)
print('The system has a total size of ' + str(system_size) + ' cells')

source_write_vector = np.zeros(system_size)
#######################################################################################################################
# Produce the linear solution for comparison
solution_linalg = spla.spsolve(kkt_mat, kkt_vec)
q_linalg = solution_linalg[:no_cells]
solution_linalg = Sh.KktVector(no_cells, cell_size, s_type, solution_linalg, 2)
print('\nThe difference between the input and the solution is ', calculate_difference(q_linalg), '\n')
input_object.write_source_to_file(solution_linalg, 'linalg', index)
sol_newt = newton()[0]

print('\nThe difference between the input and the solution is ', calculate_difference(sol_newt[:no_cells]), '\n')

print('The elapsed time is: ' + str(time.process_time()))
print('\n')

exit()
#######################################################################################################################
# Iterative system
preco = spla.LinearOperator((system_size, system_size), preconditioner_type)
solution_gmres = spla.gmres(kkt_mat, kkt_vec, tol=tolerance, restart=gmres_restart, maxiter=maximum_iterations, M=preco, callback=callback)[0]
residual = np.linalg.norm(kkt_mat.dot(solution_gmres)-kkt_vec)/np.linalg.norm(kkt_vec)
print('GMRES converged with a final residual of ', residual, 'after ', callback_index, ' iterations.')
write_x = Sh.KktVector(no_cells, cell_size, s_type, solution_gmres, 2)
input_object.write_source_to_file(write_x, 'linalg', index)

print('\nThe difference between the input and the solution is ', calculate_difference(solution_gmres[:no_cells]), '\n')
print('The elapsed time is: ' + str(time.process_time()))

sol_gmres_new = newton_gmres()[0]

print('\nThe difference between the input and the solution is ', calculate_difference(sol_gmres_new[:no_cells]), '\n')

print('End of program')
print('The elapsed time is: ' + str(time.process_time()))
exit()
#minimum_check()
solution_newton, converged = newton()
print('Newton solution converged = ', converged)
print('The elapsed time is: ' + str(time.process_time()))
#######################################################################################################################

#solution_minres, errors = Minres.solve_minres_preconditioned(kkt_mat, kkt_vec, no_cells, s_type, a, adj, alpha, cell_size, sigma_d.get_vector(), preconditioner_type=preconditioner_type, approximate=approximate, accuracy=1e-6, issparse=issparse)
print('\n')
