# Fill L_diff diffusion matrix

def build_Ldiffusion(ni,nj,Sigma_a,Dcoef):
  no_elem = ni * nj
  nnz_max = 9 * no_elem
  L_row_indices = np.zeros(nnz_max,dtype=int)
  L_col_indices = np.zeros(nnz_max,dtype=int)
  L_data = np.zeros(nnz_max)
  pointer = 0

  for j in range(nj):
    for i in range(ni):
      row = get_elem_no(i,j)

      # total removal

      val = vol * Sigma_a[i,j]

      col = row
      L_row_indices[pointer] = row
      L_col_indices[pointer] = col
      L_data[pointer] = val
      pointer = pointer + 1

      # right face

      if i < ni-1:
        val = 0.5 * (Dcoef[i,j] + Dcoef[i,j]) * dy / dx
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = +val
        pointer = pointer + 1

        col = get_elem_no(i+1,j)
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = -val
        pointer = pointer + 1
      else:
        val = dy/2
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = val
        pointer = pointer + 1

      # left face

      if i > 0:
        val = 0.5 * (Dcoef[i,j] + Dcoef[i,j]) * dy / dx
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = +val
        pointer = pointer + 1

        col = get_elem_no(i-1,j)
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = -val
        pointer = pointer + 1
      else:
        val = dy/2
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = val
        pointer = pointer + 1

      # top face

      if j < nj-1:
        val = 0.5 * (Dcoef[i,j] + Dcoef[i,j]) * dx / dy
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = +val
        pointer = pointer + 1

        col = get_elem_no(i,j+1)
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = -val
        pointer = pointer + 1
      else:
        val = dx/2
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = val
        pointer = pointer + 1

      # bottom face

      if j > 0:
        val = 0.5 * (Dcoef[i,j] + Dcoef[i,j]) * dx / dy
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = +val
        pointer = pointer + 1

        col = get_elem_no(i,j-1)
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = -val
        pointer = pointer + 1
      else:
        val = dx/2
        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = val
        pointer = pointer + 1

  # put in matrix

  L_diff = csr_matrix((L_data,(L_row_indices,L_col_indices)),shape=(no_elem,no_elem))
