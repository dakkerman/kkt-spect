There are many files in this folder. For 1D and 3D, the file of interest is Executable.py and Executable_3D.py. 
In 2D there are three different files, each for a different preconditioner, called physics_based_preconditioner.py, lup.py, alger.py. 
The files TransportND.py handle transport matrices, KKT_shape.py and KKT_solver.py contain several classes and functions used in the calculations, it's worth checking these out.
multiplot.py is used for several plotting methods, but you may want to use your own.
There are also several files containing methods written by Danny. I did not use these files directly, but they may be worth checking out, because they use some very different (and faster) techniques.
