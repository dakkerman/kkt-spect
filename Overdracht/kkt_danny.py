import numpy as np
import math
import copy
#import Ldiffusion
#import gcr
from General_Functions   import write_matrix, get_plot_number, read_matrix
from KKT_shape           import Grid2D
from detectors_danny2 import calc_det_partial_current,calc_det_collimated,calc_K_D_collimated,calc_K_D_partial_current,calc_obj_func
from dof_ordering import dof,get_elem_no
from Ltransport_danny import build_Ltransport
from scipy.sparse        import coo_matrix,csr_matrix,isspmatrix_csr,hstack,vstack,issparse,isspmatrix_csr
from scipy.sparse.linalg import spsolve
from numpy               import dot
from numpy.linalg        import solve
from numpy.linalg        import cond
from numpy.linalg        import norm
from numpy import matrix

#######################################################

def calc_scal_flux(phi):
  scalflux = np.zeros(no_elem)
  for ordinate in range (no_ordinates):
    for j in range (nj):
      for i in range (ni):
        phi_index = dof(ordinate,i,j,ni,nj)
        scal_index = get_elem_no(i,j,ni)
        scalflux[scal_index] = scalflux[scal_index] + weights[ordinate] * phi[phi_index]
  return scalflux

#######################################################

# Input parameters

Lx = 1.0
Ly = 1.0

ni = 8
nj = 8

Sigma_t = np.zeros((ni,nj))
Sigma_t[:,:] = 1.0

Sigma_s = np.zeros((ni,nj))
Sigma_s = 0.3 * Sigma_t

no_ordinates = 4

alpha_kkt = 1.0E-6

src_start = 2
src_end   = 5

#######################################################

Dcoef = 1/(3*Sigma_t)
Sigma_a = Sigma_t - Sigma_s

omegas = np.zeros((no_ordinates,2))
weights = (4 * math.pi / no_ordinates) * np.ones(no_ordinates)
#for ordinate in range(no_ordinates):
#  theta = (math.pi/4) + ordinate * 2*math.pi/no_ordinates
#  omegas[ordinate,0] = math.cos(theta)
#  omegas[ordinate,1] = math.sin(theta)

omegas[0,0] = + 1.0 / math.sqrt(3)
omegas[0,1] = + 1.0 / math.sqrt(3)

omegas[1,0] = - 1.0 / math.sqrt(3)
omegas[1,1] = + 1.0 / math.sqrt(3)

omegas[2,0] = - 1.0 / math.sqrt(3)
omegas[2,1] = - 1.0 / math.sqrt(3)

omegas[3,0] = + 1.0 / math.sqrt(3)
omegas[3,1] = - 1.0 / math.sqrt(3)

dx = Lx / ni
dy = Ly / nj
vol = dx * dy

no_elem = ni*nj
L_no_dof = no_ordinates * no_elem

# Fill alpha_x_M (alpha_kkt*vol on main diagonal)

alpha_x_M = csr_matrix((no_elem,no_elem))
csr_matrix.setdiag(alpha_x_M,alpha_kkt * vol * np.ones(no_elem))

#print ('alpha_x_M',alpha_x_M)

# Fill C (- weight * vol * I stacked vertically)

length = no_ordinates*no_elem
rows = np.arange(length)
cols = np.zeros(length,dtype=int)
data = np.zeros(length)

for ordinate in range(no_ordinates):
  start = ordinate*no_elem
  end   = (ordinate+1)*no_elem-1
  cols[start:end+1] = np.arange(no_elem,dtype=int)
  data[start:end+1] = - weights[ordinate] * vol * np.ones(no_elem) / (4 * math.pi)

C = csr_matrix((data,(rows,cols)),shape=(L_no_dof,no_elem))

#print ('C',C)

C_T = C.transpose()

L = build_Ltransport(ni,nj,no_ordinates,omegas,weights,dx,dy,Sigma_t,Sigma_s)

#print ('L',L)

L_T = L.transpose()

K_D_collimated      = calc_K_D_collimated(ni,nj,dx,dy,no_ordinates)
K_D_partial_current = calc_K_D_partial_current(ni,nj,dx,dy,no_ordinates,omegas,weights)

#print ('K_D_collimated  ',K_D_collimated)
#print ('K_D_part_current',K_D_partial_current)

#######################################################

# Reference flux and detector response

src_ref_iso_2d = np.zeros((ni,nj))
src_ref_iso_2d[src_start:src_end+1,src_start:src_end+1] = 1.0
src_ref_iso = src_ref_iso_2d.reshape(no_elem)

src_ref = - C.dot(src_ref_iso)
phi_ref = spsolve(L,src_ref)
scalflux_ref = calc_scal_flux(phi_ref)

#plotfun = scalflux_ref.reshape(ni,nj)
#plt.imshow(plotfun)
#plt.colorbar()
#plt.show()

#exit()

#(det_right,det_left,det_top,det_bottom) = calc_det_partial_current(ni,nj,no_ordinates,omegas,weights,phi_ref)
(det_right,det_left,det_top,det_bottom) = calc_det_collimated(ni,nj,phi_ref)

#print ("det_right",det_right)
#print ("det_left",det_left)
#print ("det_top",det_top)
#print ("det_bottom",det_bottom)

#######################################################

# (M xi,D) for the collimated detector

rhs_phi_adj = np.zeros(L_no_dof)

for j in range(nj):
  ordinate1 = 0
  phi_index = dof(ordinate1,ni-1,j,ni,nj)
  rhs_phi_adj[phi_index] += dy * 0.5 * det_right[j]

  ordinate2 = 3
  phi_index = dof(ordinate2,ni-1,j,ni,nj)
  rhs_phi_adj[phi_index] += dy * 0.5 * det_right[j]

  ordinate1 = 1
  phi_index = dof(ordinate1,0,j,ni,nj)
  rhs_phi_adj[phi_index] += dy * 0.5 * det_left[j]

  ordinate2 = 2
  phi_index = dof(ordinate2,0,j,ni,nj)
  rhs_phi_adj[phi_index] += dy * 0.5 * det_left[j]

for i in range(ni):
  ordinate1 = 0
  phi_index = dof(ordinate1,i,nj-1,ni,nj)
  rhs_phi_adj[phi_index] += dx * 0.5 * det_top[i]

  ordinate2 = 1
  phi_index = dof(ordinate2,i,nj-1,ni,nj)
  rhs_phi_adj[phi_index] += dx * 0.5 * det_top[i]

  ordinate1 = 2
  phi_index = dof(ordinate1,i,0,ni,nj)
  rhs_phi_adj[phi_index] += dx * 0.5 * det_bottom[i]

  ordinate2 = 3
  phi_index = dof(ordinate2,i,0,ni,nj)
  rhs_phi_adj[phi_index] += dx * 0.5 * det_bottom[i]

#######################################################

# (M xi,D) for the outward current detector

#rhs_phi_adj = np.zeros(L_no_dof)
#
#for ordinate in range (no_ordinates):
#  omega = omegas[ordinate]
#  for j in range(nj):
#    if omega[0] > 0.0:
#      phi_index = dof(ordinate,ni-1,j,ni,nj)
#      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] + dy * weights[ordinate] * omega[0] * det_right[j]
#    if omega[0] < 0.0:
#      phi_index = dof(ordinate,0,j,ni,nj)
#      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] - dy * weights[ordinate] * omega[0] * det_left[j]
#  for i in range(ni):
#    if omega[1] > 0.0:
#      phi_index = dof(ordinate,i,nj-1,ni,nj)
#      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] + dx * weights[ordinate] * omega[1] * det_top[i]
#    if omega[1] < 0.0:
#      phi_index = dof(ordinate,i,0,ni,nj)
#      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] - dx * weights[ordinate] * omega[1] * det_bottom[i]

#######################################################

A12zero = csr_matrix((no_elem,L_no_dof))
csr_matrix.setdiag(A12zero,np.zeros(no_elem))

A21zero = csr_matrix((L_no_dof,no_elem))
csr_matrix.setdiag(A21zero,np.zeros(no_elem))

A33zero = csr_matrix((L_no_dof,L_no_dof))
csr_matrix.setdiag(A33zero,np.zeros(L_no_dof))

#######################################################

# Construct complete KKT system

kkt_row1 = hstack((alpha_x_M,A12zero,C_T))
kkt_row2 = hstack((A21zero,K_D_collimated,L_T))
kkt_row3 = hstack((C,L,A33zero))

kkt_mat = vstack((kkt_row1,kkt_row2,kkt_row3)).tocsr()

del kkt_row1
del kkt_row2
del kkt_row3

#######################################################

rhs_q       = np.zeros(no_elem)
rhs_phi_fwd = np.zeros(L_no_dof)

phi_adj_ref = np.zeros(L_no_dof)

kkt_sol_ref = np.concatenate((src_ref_iso,phi_ref,phi_adj_ref))

# KKT rhs

kkt_rhs = np.concatenate((rhs_q,rhs_phi_adj,rhs_phi_fwd))

# Solve KKT system

kkt_sol = spsolve(kkt_mat,kkt_rhs)

q_recon = kkt_sol[0:no_elem]
print(q_recon)
q_write = Grid2D(q_recon)
with open('testq.txt', 'w') as f:
    write_matrix(q_write.get_matrix(), f) 

# Plot source reconstruction

"""
plotfun = q_recon.reshape(ni,nj)
plt.imshow(plotfun)
plt.colorbar()
plt.show()
"""

# Print the objectve function at the optimal solution

src_opt = - C.dot(q_recon)
phi_opt = spsolve(L,src_opt)
(det_right_opt,det_left_opt,det_top_opt,det_bottom_opt) = calc_det_collimated(ni,nj,phi_opt)
objective_min = calc_obj_func(alpha_kkt,ni,nj,dx,dy,q_recon,det_right_opt,det_left_opt,det_top_opt,det_bottom_opt,det_right,det_left,det_top,det_bottom)
print('objective_min',objective_min)

index_dav = get_plot_number(increment=False) - 1
filename_dav = 'Matrices/'
filename_dav += str(index_dav) + '_'
filename_dav += 'N=' + str(ni) + '_'
filename_dav += 'L=' + str(1) + '_'
filename_dav += 's=' + str(2) + '_'
filename_dav += 'alph=1e-06_normal' 
filename_dav += '.txt'
q_dav = read_matrix(filename_dav, create_object=False).flatten()
src_opt_dav = - C.dot(q_dav)
phi_opt_dav = spsolve(L,src_opt_dav)
(det_right_dav, det_left_dav, det_top_dav, det_bottom_dav) = calc_det_collimated(ni, nj, phi_opt_dav)
objective_min_dav = calc_obj_func(alpha_kkt, ni, nj, dx, dy, q_dav, det_right_dav, det_left_dav, det_top_dav, det_bottom_dav, det_right, det_left, det_top, det_bottom)
print('objective_min david', objective_min_dav)
# Perturb q_recon in all elements and see check the objective function (should increase)

for j in range (nj):
  for i in range (ni):

    q_pert = copy.deepcopy(q_recon)
    index = get_elem_no(i,j,ni)
    q_pert[index] += 1.0e-3
    src_pert = - C.dot(q_pert)

    phi_pert = spsolve(L,src_pert)
    (det_right_pert,det_left_pert,det_top_pert,det_bottom_pert) = calc_det_collimated(ni,nj,phi_pert)
    objective_pert = calc_obj_func(alpha_kkt,ni,nj,dx,dy,q_pert,det_right_pert,det_left_pert,det_top_pert,det_bottom_pert,det_right,det_left,det_top,det_bottom)
    print('i j obj_pert-obj_min',i,j,objective_pert-objective_min)

