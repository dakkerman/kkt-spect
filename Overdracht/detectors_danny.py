import numpy as np
from dof_ordering import dof
from scipy.sparse import csr_matrix
from numpy import inner

#######################################################

def calc_obj_func(alpha,ni,nj,dx,dy,q,det_right,det_left,det_top,det_bottom,det_right_meas,det_left_meas,det_top_meas,det_bottom_meas):

  obj_func = 0.0

  for j in range(nj):
    obj_func += 0.5 * dy * (det_right[j] - det_right_meas[j])**2
    obj_func += 0.5 * dy * (det_left[j]  - det_left_meas[j] )**2

  for i in range(ni):
    obj_func += 0.5 * dx * (det_top[i]    - det_top_meas[i]   )**2
    obj_func += 0.5 * dx * (det_bottom[i] - det_bottom_meas[i])**2

  # regularization term

  obj_func += 0.5 * alpha * dx * dy * inner(q,q)

  return obj_func

#######################################################

def calc_det_partial_current(ni,nj,no_ordinates,omegas,weights,phi):
  det_right  = np.zeros(nj)
  det_left   = np.zeros(nj)
  det_top    = np.zeros(ni)
  det_bottom = np.zeros(ni)

  for ordinate in range (no_ordinates):
    omega = omegas[ordinate]
    for j in range(nj):
      if omega[0] > 0.0:
        phi_index = dof(ordinate,ni-1,j,ni,nj)
        det_right[j]  += + weights[ordinate] * omega[0] * phi[phi_index]
      if omega[0] < 0.0:
        phi_index = dof(ordinate,0,j,ni,nj)
        det_left[j]   += - weights[ordinate] * omega[0] * phi[phi_index]
    for i in range(ni):
      if omega[1] > 0.0:
        phi_index = dof(ordinate,i,nj-1,ni,nj)
        det_top[i]    += + weights[ordinate] * omega[1] * phi[phi_index]
      if omega[1] < 0.0:
        phi_index = dof(ordinate,i,0,ni,nj)
        det_bottom[i] += - weights[ordinate] * omega[1] * phi[phi_index]

  return det_right, det_left, det_top, det_bottom

#######################################################

def calc_det_collimated(ni,nj,phi):
  det_right  = np.zeros(nj)
  det_left   = np.zeros(nj)
  det_top    = np.zeros(ni)
  det_bottom = np.zeros(ni)

  for j in range(nj):
      ordinate1 = 0
      ordinate2 = 3
      phi1 = phi[dof(ordinate1,ni-1,j,ni,nj)]
      phi2 = phi[dof(ordinate2,ni-1,j,ni,nj)]
      phi_coll = (phi1 + phi2) / 2
      det_right[j]  += phi_coll

      ordinate1 = 1
      ordinate2 = 2
      phi1 = phi[dof(ordinate1,0,j,ni,nj)]
      phi2 = phi[dof(ordinate2,0,j,ni,nj)]
      phi_coll = (phi1 + phi2) / 2
      det_left[j]   += phi_coll
  for i in range(ni):
      ordinate1 = 0
      ordinate2 = 1
      phi1 = phi[dof(ordinate1,i,nj-1,ni,nj)]
      phi2 = phi[dof(ordinate2,i,nj-1,ni,nj)]
      phi_coll = (phi1 + phi2) / 2
      det_top[i]    += phi_coll

      ordinate1 = 2
      ordinate2 = 3
      phi1 = phi[dof(ordinate1,i,0,ni,nj)]
      phi2 = phi[dof(ordinate2,i,0,ni,nj)]
      phi_coll = (phi1 + phi2) / 2
      det_bottom[i] += phi_coll

  return det_right, det_left, det_top, det_bottom

#######################################################

# Fill K_D_partial_current matrix

def calc_K_D_partial_current(ni,nj,dx,dy,no_ordinates,omegas,weights):
  no_elem = ni * nj
  L_no_dof = no_ordinates * no_elem
  nnz_max = (no_ordinates**2) * 2 * (ni + nj)
  K_row_indices = np.zeros(nnz_max,dtype=int)
  K_col_indices = np.zeros(nnz_max,dtype=int)
  K_data = np.zeros(nnz_max)
  pointer = 0

  for ordinate in range(no_ordinates):
    omega = omegas[ordinate]
    for ordinate2 in range(no_ordinates):
      omega2 = omegas[ordinate2]

      for j in range(nj):

        if omega[0] > 0.0 and omega2[0] > 0.0:
          i=ni-1
          row = dof(ordinate,i,j,ni,nj)
          col = dof(ordinate2,i,j,ni,nj)
          val = dy * (weights[ordinate] * omega[0]) * (weights[ordinate2] * omega2[0])
          K_row_indices[pointer] = row
          K_col_indices[pointer] = col
          K_data[pointer] = val
          pointer = pointer + 1

        if omega[0] < 0.0 and omega2[0] < 0.0:
          i=0
          row = dof(ordinate,i,j,ni,nj)
          col = dof(ordinate2,i,j,ni,nj)
          val = dy * (weights[ordinate] * omega[0]) * (weights[ordinate2] * omega2[0])
          K_row_indices[pointer] = row
          K_col_indices[pointer] = col
          K_data[pointer] = val
          pointer = pointer + 1

      for i in range(ni):

        if omega[1] > 0.0 and omega2[1] > 0.0:
          j=nj-1
          row = dof(ordinate,i,j,ni,nj)
          col = dof(ordinate2,i,j,ni,nj)
          val = dx * (weights[ordinate] * omega[1]) * (weights[ordinate2] * omega2[1])
          K_row_indices[pointer] = row
          K_col_indices[pointer] = col
          K_data[pointer] = val
          pointer = pointer + 1

        if omega[1] < 0.0 and omega2[1] < 0.0:
          j=0
          row = dof(ordinate,i,j,ni,nj)
          col = dof(ordinate2,i,j,ni,nj)
          val = dx * (weights[ordinate] * omega[1]) * (weights[ordinate2] * omega2[1])
          K_row_indices[pointer] = row
          K_col_indices[pointer] = col
          K_data[pointer] = val
          pointer = pointer + 1

  K_D_partial_current = csr_matrix((K_data,(K_row_indices,K_col_indices)),shape=(L_no_dof,L_no_dof))

  return K_D_partial_current

#######################################################

# Fill K_D_collimated matrix

def calc_K_D_collimated(ni,nj,dx,dy,no_ordinates):
  no_elem = ni * nj
  L_no_dof = no_ordinates * no_elem
  nnz_max = (no_ordinates**2) * 2 * (ni + nj)
  K_row_indices = np.zeros(nnz_max,dtype=int)
  K_col_indices = np.zeros(nnz_max,dtype=int)
  K_data = np.zeros(nnz_max)
  pointer = 0

  for j in range(nj):
    i=ni-1
    val = dy * 0.25
    for ordinate1 in [0,3]:
      for ordinate2 in [0,3]:
        row = dof(ordinate1,i,j,ni,nj)
        col = dof(ordinate2,i,j,ni,nj)
        K_row_indices[pointer] = row
        K_col_indices[pointer] = col
        K_data[pointer] = val
        pointer = pointer + 1

    i=0
    val = dy * 0.25
    for ordinate1 in [1,2]:
      for ordinate2 in [1,2]:
        row = dof(ordinate1,i,j,ni,nj)
        col = dof(ordinate2,i,j,ni,nj)
        K_row_indices[pointer] = row
        K_col_indices[pointer] = col
        K_data[pointer] = val
        pointer = pointer + 1

  for i in range(ni):
    j=nj-1
    val = dx * 0.25
    for ordinate1 in [0,1]:
      for ordinate2 in [0,1]:
        row = dof(ordinate1,i,j,ni,nj)
        col = dof(ordinate2,i,j,ni,nj)
        K_row_indices[pointer] = row
        K_col_indices[pointer] = col
        K_data[pointer] = val
        pointer = pointer + 1

    j=0
    val = dx * 0.25
    for ordinate1 in [2,3]:
      for ordinate2 in [2,3]:
        row = dof(ordinate1,i,j,ni,nj)
        col = dof(ordinate2,i,j,ni,nj)
        K_row_indices[pointer] = row
        K_col_indices[pointer] = col
        K_data[pointer] = val
        pointer = pointer + 1

  K_D_collimated = csr_matrix((K_data,(K_row_indices,K_col_indices)),shape=(L_no_dof,L_no_dof))

  return K_D_collimated

#######################################################
