import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


def plot(x_axis, data, rows=1, cols=1):
    if rows != 1 or cols != 1:
        k = 1
        new_subplot(rows, cols, k)
    try:
        for i in range(len(data)):
            if data[i] is None:
                k += 1
                new_subplot(rows, cols, k)
            elif type(data[i]) is not str:
                if i < (len(data) -1):
                    if type(data[i+1]) is str:
                        plt.plot(x_axis, data[i], data[i+1].split(':')[0], label=data[i+1].split(':')[1])
                else:
                    plt.plot(x_axis, data[i])
    except ValueError:
        plt.plot(x_axis, data)
    else:
        plt.legend()
    plt.show()


def new_subplot(rows, cols, k):
    if k > 1:
        plt.legend()
    plt.subplot(rows, cols, k)


def plot_2d(n, l, z, title=None):
    x = np.linspace(0, l, n)
    y = x
    x, y = np.meshgrid(x, y)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(x, y, z, cmap=cm.coolwarm, linewidth=0, antialiased=False)
    if title is not None:
        ax.set_title(title)
    fig.show()


def plot_errors(errors):
    fig = plt.figure()
    ax = fig.gca()
    ax.plot(np.linspace(0, errors.size, errors.size), errors)
    plt.xlabel('Number of iterations')
    plt.ylabel('Error')
    plt.yscale('log')
    fig.show()
