import numpy as np
import multiplot as mplt
import matplotlib.pyplot as plt
import source
import KKT_solver as Solver
import time
import Transport1D as Tp

# Initialization of constants
N_cells = 100  # Number of cells in de x-direction
L = 1
delta_x = L / N_cells
alpha = 1E-6
isotropic = False
reduced = False
x_plot = np.linspace(0, L, N_cells)

absorption_xsec = source.Source(0, N_cells, L, 0.5, 0.5).get_source()  # Initialization of absorption cross section
scatter_xsec = source.Source(0, N_cells, L, 0.5, 0.5).get_source()  # Initialization of scatter cross section
Src = source.Source(3, N_cells, L, 0.2, 0.1)
Det_resp = source.Source(0, N_cells, L)
# Src = source.Source.initialize_source(N_cells, L, 'source') # Lets the user choose its own type of source#
# Det_resp = source.Source.initialize_source(N_cells, L, 'detector')  # Lets the user choose its own type of detector

(phi_plus, phi_min, system_matrix) = Tp.calculate_phi(N_cells, delta_x, absorption_xsec, scatter_xsec, Src)
phi = 2*np.pi*(phi_plus + phi_min)
phi_detected = np.multiply(Det_resp.get_source(), np.concatenate((phi_plus, phi_min)))

(phi_adjoint_plus, phi_adjoint_min, adjoint_matrix) = Tp.calculate_adjoint(N_cells, delta_x,
                                                                           absorption_xsec, scatter_xsec, Det_resp)

# Create the KKT-system matrix and vector
print('Creating KKT-system')
if isotropic:
    kkt_matrix, kkt_vector = Solver.create_isotropic_kkt_system(2, alpha, system_matrix, adjoint_matrix, N_cells,
                                                                Det_resp.get_source(), phi_detected, isotropic)
elif reduced:
    kkt_matrix, kkt_vector = Solver.create_reduced_kkt_system(alpha, system_matrix, adjoint_matrix, N_cells,
                                                              Det_resp.get_source(), phi_detected)
else:
    kkt_matrix, kkt_vector = Solver.create_kkt_system(2, alpha, system_matrix, adjoint_matrix, N_cells,
                                                      Det_resp.get_source(), phi_detected, isotropic)

# Solve the KKT-system with Solver.solve(alpha, a, a_adj, n: int, delta_x: int, sigma_d, detector)
print('Solving directly')
result = Solver.solve(kkt_matrix, kkt_vector)
if not isotropic:
    source_calculated = result[0:2*N_cells]
    phi_calculated = result[2 * N_cells:(4 * N_cells)]
    phi_adjoint_calculated = result[(4 * N_cells):(6 * N_cells)]
else:
    source_calculated = result[0:N_cells]
    phi_calculated = result[N_cells:(3 * N_cells)]
    phi_adjoint_calculated = result[(3 * N_cells):(5 * N_cells)]
# """

# Solve the system with the MINRES method
print('Solving by using MINRES')
minres_solution, errors = Solver.solve_minres_preconditioned(kkt_matrix, kkt_vector, preconditioner_type='diag',
                                                             approximate=True)

if isotropic:
    source_iterative = minres_solution[0:N_cells]
    phi_iterative = minres_solution[N_cells:3*N_cells]
else:
    source_iterative = minres_solution[0:2*N_cells]
    phi_iterative = minres_solution[2*N_cells:4*N_cells]


def plot_results():
    plt.subplot(211)
    plt.plot(x_plot, Src.get_source(half=1), label='q+')
    plt.plot(x_plot, Src.get_source(half=2), label='q-')
    #plt.plot(x_plot, source_calculated[0:N_cells], '--', label='source calculated +')
    #if not isotropic:
    #    plt.plot(x_plot, source_calculated[N_cells:2*N_cells], '--', label='source calculated -')
    #plt.plot(x_plot, Det_resp.get_source(half=1), dashes=[1, 2], label='detector response')
    plt.plot(x_plot, source_iterative[0:N_cells], '--', label='Iterative+')
    if not isotropic:
        plt.plot(x_plot, source_iterative[N_cells:2*N_cells], '--', label='Iterative-')
    plt.legend()
    plt.subplot(212)
    plt.plot(x_plot, phi_plus, label='phi+')
    plt.plot(x_plot, phi_min, label='phi-')
    # plt.plot(x_plot, phi_calculated[0:N_cells], '--', label='phi calculated+')
    # plt.plot(x_plot, phi_calculated[N_cells:2*N_cells], '--', label='phi calculated-')
    plt.plot(x_plot, phi_iterative[0:N_cells], '--', label='Iterative+')
    plt.plot(x_plot, phi_iterative[N_cells:2*N_cells], '--', label='Iterative-')
    plt.legend()
    plt.show()


mplt.plot_errors(errors)


print(time.process_time())

plot_results()
mplt.plot_errors(errors)

plt.show()