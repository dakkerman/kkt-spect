import numpy as np
import math
import General_Functions as Gen
from scipy import sparse
from scipy.sparse import linalg as spalg
import KKT_shape as Sh

def create_kkt_system(no_dim, s_type, delta_x, alpha, a, n: int, sigma_d, detector, a_adj=None):
    print('Creating KKT-system')
    no_cells = n**no_dim
    cell_size = delta_x**no_dim
    directions = Sh.get_number_directions(no_dim, s_type)
    sz = (1 + 2*directions) * no_cells
    direction_vectors = Sh.initialize_directions(no_dim, s_type)
    normals = Sh.initialize_normals(no_dim)
    mat = sparse.lil_matrix((sz, sz))
    mat[0:no_cells, 0:no_cells] = 4 * np.pi * cell_size * alpha * sparse.identity(no_cells)
    vec = np.zeros(sz)
    for k in range(directions):  # When Q is isotropic, the relationship with phi_t and phi is no longer identity
        weight = direction_vectors[k].get_weight()
        dirvec = direction_vectors[k].get_vector()
        if no_dim ==3:
            weight *= .5
        for i in range(no_cells):
            mat[i, (directions+1+k)*no_cells + i] = -np.pi*weight* cell_size  # C^T
            mat[(directions+1+k)*no_cells + i, i] = -np.pi*weight* cell_size  # C
            index = i + k*n 
            #mat[(n + index), (n + index)] = weight * np.pi * cell_size * sigma_d[index] ** 2  # K
            #vec[n + index] = weight * np.pi * cell_size * sigma_d[index] * detector[index]
            #vec[n:(directions+1)*n] = np.pi*cell_size*sigma_d.dot(detector)
            #vec[n:(directions+1)*no_cells] = 0.5*delta_x*detector
        """ 
        for i in range(n):
            for p in range(4):
                normal = normals[p].get_vector()
                if np.dot(normal, dirvec) > 0:
                    if p == 0:
                        indices = np.array([0, i])
                    elif p == 1:
                        indices = np.array([i, n-1])
                    elif p == 2:
                        indices = np.array([n-1, i])
                    elif p == 3:
                        indices = np.array([i, 0])
                    index = no_cells + Sh.get_vector_index(n, indices[0], indices[1], direction=k, no_dim=no_dim)
                    det_index = p*n + i 
                    vec[index] = 0.5 * delta_x * detector[det_index]"""
    vec[no_cells:(directions+1)*no_cells] = detector
    mat[((directions+1) * no_cells):((2*directions+1) * no_cells), no_cells:((directions+1) * no_cells)] = a
    mat[no_cells:(directions+1)*no_cells, no_cells:(directions+1)*no_cells] = sigma_d.tocsr()
    """
    Gen.write_matrix_to_textfile(mat[((directions+1) * n):((2*directions+1) * n), n:((directions+1) * n)].toarray(), 'a3')
    Gen.write_matrix_to_textfile(mat[(1+directions)*n:, :n].toarray(), 'C')
    Gen.write_matrix_to_textfile(mat[:n, :n].toarray(), 'H')
    Gen.write_matrix_to_textfile(mat[n:(1+directions)*n, n:(1+directions)*n].toarray(), 'K')
    """
    if a_adj is None:
        print('Transposing A')
        a = a.tocoo()
        mat[no_cells:((directions + 1) * no_cells), ((directions + 1) * no_cells):((2 * directions + 1) * no_cells)] = a.transpose()
    else:
        mat[no_cells:((directions + 1) * no_cells), ((directions + 1) * no_cells):((2 * directions + 1) * no_cells)] = a_adj
    mat = mat.tocsr()
    print('KKT-system is ready')
    return mat, vec, sz


def create_kkt_vector(no_dim, s_type, cell_size, alpha, a, n: int, sigma_d, detector, a_adj=None):
    print('Creating KKT-vector')
    directions = Sh.get_number_directions(no_dim, s_type)
    sz = (1 + 2*directions) * n
    direction_vectors = Sh.initialize_directions(no_dim, s_type)
    vec = np.zeros(sz)
    for k in range(directions):  # When Q is isotropic, the relationship with phi_t and phi is no longer identity
        weight = direction_vectors[k].get_weight()
        if no_dim ==3:
            weight *= .5
        for i in range(n):
            index = i + k*n 
            vec[n + index] = weight * np.pi * cell_size * sigma_d[index] * detector[index]
    print('KKT-vector is ready')
    return vec, sz


def create_isotropic_kkt_system2(directions, cell_size, alpha, a, n: int, sigma_d, detector, a_adj=None):
    sz = (1 + 2*directions) * n
    mat = np.zeros((sz, sz))
    vec = np.zeros(sz)
    mat[0:n, 0:n] = alpha * np.identity(n)
    for i in range(n):
        for k in range(directions):  # When Q is isotropic, the relationship with phi_t and phi is no longer identity
            mat[i, (directions+1+k)*n + i] = -1
            mat[(directions+1+k)*n + i, i] = -1
    for i in range(directions*n):
        mat[(n + i), (n + i)] = sigma_d[i] ** 2
        vec[n + i] = sigma_d[i] * detector[i]
    if a_adj is None:
        mat[n:((directions + 1) * n), ((directions + 1) * n):((2 * directions + 1) * n)] = np.transpose(a)
    else:
        mat[n:((directions+1) * n), ((directions+1) * n):((2*directions+1) * n)] = a_adj
    mat[((directions+1) * n):((2*directions+1) * n), n:((directions+1) * n)] = a
    return mat, vec


def solve_newton(n, sz, mat, vec, x0, nu0):
    x = x0
    for i in range(10):
        x = newton_step(n, sz, mat, vec, x, nu0)
        nu0 *= 0.5
    return x


def newton_step(n, sz, directions, mat, vec, xk, nu):
    """
    :param n: The size of the 'q' block. In principle this is #directions * N^2
    :param sz: The total size of the system
    :param directions: Number of direction ordinates
    :param mat:
    :param vec: Right vector - (0, Sigma_d*R, 0)^T
    :param xk: x_k
    :param nu: logarithmic barrier strength
    :return: x_k+1
    """
    mat.tolil()
    logmat = np.zeros(sz)
    J = sparse.lil_matrix(mat)
    for i in range(n):
        J[i, i] += nu/(xk[i]**2)
        logmat[i] = nu/xk[i]
    J = J.tocsr()
    mat = mat.tocsr()
    delta_x = spalg.spsolve(J, -mat.dot(xk) + logmat + vec)
    converged = False
    s = 1
    while not converged:
        for i in range(sz):
            if xk[i] + s*delta_x[i] <= 0:
                s *= 0.9
                break
            converged = True
    print(np.linalg.norm(delta_x)/np.linalg.norm(xk))
    return xk + s*delta_x, np.linalg.norm(delta_x)/np.linalg.norm(xk) < 1e-4 


def mv_product(no_cells, sz, s_type, alpha, cell_size, Sigma_D, a, adj, vec):
    Z = Sh.get_number_directions(2, s_type)
    res = np.zeros(sz)
    direction_vectors = Sh.initialize_directions(2, s_type)
    k = 0  # Counter for keeping track of Omega in the middle block
    j = 0
    for i in range(sz):
        temp = 0
        if i < no_cells:
            #temp += 4*np.pi*alpha*cell_size*vec[i]
            temp += 4*np.pi*alpha*cell_size*vec[i]
            for p in range(Z):
                weight = direction_vectors[p].get_weight()
                temp -= weight*np.pi*cell_size*vec[i + (1+Z+p)*no_cells]
        elif i < (1+Z)*no_cells:
            if j == no_cells:
                j = 0
                k += 1
                weight = direction_vectors[k].get_weight()
            temp += weight*np.pi*cell_size*(Sigma_D[i-no_cells]**2)*vec[i]
            temp += adj[i-no_cells, :].dot(vec[(1+Z)*no_cells:])
            j += 1
        else:
            if j == no_cells:
                j = 0
                if k == Z-1:
                    k = 0
                else:
                    k += 1
                weight = direction_vectors[k].get_weight()
            temp -= weight*np.pi*cell_size*vec[i-(1+Z)*no_cells - k*no_cells]
            temp += a[i-(Z+1)*no_cells].dot(vec[no_cells:(1+Z)*no_cells])
            j += 1
        res[i] = temp
    return res
