import numpy as np
import scipy as sp
import KKT_shape as Sh


class KktCalculations:
    def __init__(self, n: int, l, s_type: int, alpha, absorption, scatter):
        self.n = n
        self.l = l
        self.s_type = s_type
        self.alpha = alpha
        self.absorption = absorption
        self.scatter = scatter

    def get_number_cells(self):
        return self.n

    def get_length(self):
        return self.l

    def get_s(self):
        return self.s_type

    def get_alpha(self):
        return self.alpha

    def get_absorption(self):
        return self.absorption

    def get_scatter(self):
        return self.scatter

    def write_source_to_file(self, matrix: Sh.KktVector, type: str, index):
        filename = 'Matrices/'
        filename += str(index) + '_'
        filename += 'N=' + str(self.n) + '_'
        filename += 'L=' + str(self.l) + '_'
        filename += 's=' + str(self.s_type) + '_'
        filename += 'alph=' + '{:.0e}'.format(self.alpha) + '_'
        filename += type
        filename += '.txt'
        ###
        print('Writing to file ' + filename)
        with open(filename, 'w+') as f:
            write_matrix(matrix.get_q(), f, approximate=False)

    def write_matrix_to_file(self, matrix: np.ndarray, type: str, index):
        filename = 'Matrices/'
        filename += str(index) + '_'
        filename += 'N=' + str(self.n) + '_'
        filename += 'L=' + str(self.l) + '_'
        filename += 's=' + str(self.s_type) + '_'
        filename += 'alph=' + '{:.0e}'.format(self.alpha) + '_'
        filename += type
        filename += '.txt'
        ###
        print('Writing to file ' + filename)
        with open(filename, 'w+') as f:
            write_matrix(matrix, f, approximate=False)



########################################################################################################################
def get_plot_number(increment: bool=True):
    with open('Matrices/index.txt', 'r') as f:
        index = int(f.read())
        f.close()
    if increment:
        with open('Matrices/index.txt', 'w') as f:
            f.write(str(index+1))
            f.close()
    return index


def compile_row_string(a_row):
    return str(a_row).strip(']').strip('[').replace(' ', '')


def write_matrix(mat, file_to_write, approximate=True, delete_zeros=False):
    for row in mat:
        if type(row) is np.ndarray:
            for item in row:
                if delete_zeros and item == 0:
                    file_to_write.write('.\t')
                else:
                    if approximate:
                        """if 0.1 < abs(item) < 10 or item == 0:
                            file_to_write.write('{:.1f}'.format(item) + '\t')
                        else:
                            file_to_write.write('{:.1e}'.format(item) + '\t')
                        """
                        file_to_write.write(str(item)+'\t')
                    else:
                        file_to_write.write(str(item) + '\t')
            file_to_write.write('\n')
        else:
            if delete_zeros and row == 0:
                file_to_write.write('.\t')
            else:
                if approximate:
                    if 0.1 < abs(row) < 10 or row == 0:
                        file_to_write.write('{:.1f}'.format(row) + '\t')
                    else:
                        file_to_write.write('{:.1e}'.format(row) + '\t')
                else:
                    file_to_write.write(str(row) + '\n')


def write_matrix_to_textfile(a_matrix, filename: str, write_filename: bool=True):
    print('Writing to textfile ', filename)
    filename = 'Matrices/' + filename + '.txt'
    with open(filename, 'w+') as f:
        if write_filename:
            f.write(filename + '\n')
        write_matrix(a_matrix, f)
    f.close()


def read_matrix(filename: str, create_object: bool):
    if create_object:
        entries = filename.split('_')
        for i in range(1, entries.__len__()):
            entries[i] = entries[i].split('=')[1]
            if i == entries.__len__() - 1:
                entries[i] = entries[i].split('.')[0]
        system_input = KktCalculations(int(entries[1]), float(entries[2]), int(entries[3]), float(entries[4]),
                                       float(entries[5]), float(entries[6]), bool(entries[7]))
        n = system_input.get_number_cells()
    else:
        entries = filename.split('=')
        n = int(entries[1].split('_')[0])
    with open(filename) as f:
        lines = f.readlines()
    mat = string_to_matrix(lines, n)
    try:
        return mat, system_input
    except UnboundLocalError:
        return mat


def string_to_matrix(lines, n: int):
    m = max(lines[0].split('\t').__len__()-1, 1)
    mat = np.zeros((n, m))
    for i in range(n):
        row = lines[i]
        if m > 1:
            splitted_string = row.split('\t')
            for j in range(m):
                try:
                    mat[i, j] = float(splitted_string[j])
                except IndexError:
                    print('item is ' + ' ' + str(i) + ',' + str(j))
        else:
            mat[i] = float(row)
    return mat


def write_errors_to_file(errors, index):
    filename = 'Matrices/'
    filename += str(index)
    filename += '_N=' + str(np.size(errors))
    filename += '_errors.txt'
    print('writing errors to ' + filename)
    with open(filename, 'w+') as f:
        write_matrix(errors, f, approximate=False)


def print_2tensor(mat, l):
    for i in range(l):
        vec = mat[i, :]
        line = ''
        for item in vec:
            line += '{:.1e}'.format(item) + '  \t'
        print(line)


def print_3tensor(mat, l):
    for i in range(l):
        for j in range(l):
            vec = mat[i, j, :]
            line = ''
            for item in vec:
                line += '{:.1e}'.format(item) + '  \t'
            print(line)
        print('-')


