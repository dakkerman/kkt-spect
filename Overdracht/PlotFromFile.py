import matplotlib.pyplot as plt
import multiplot as mplt
import General_Functions as Gen
import numpy as np

no_plots = 1
filenumber = 1421
N = 16
S = 4
for i in range(no_plots):
    if i < 1:
        filename = str(filenumber) + '_N=' + str(N) + '_L=1_s=' + str(S) + '_alph=1e-04_normal'# + str(3*i)
    else:
        filename = str(filenumber) + '_N=' + str(N) + '_L=1_s=' + str(S) + '_alph=1e-06_normal2'
    filename = 'Matrices/' + filename + '.txt'
    if 'error' in filename:
        vec = Gen.read_matrix(filename, create_object=False)
        x = np.arange(np.size(vec))
        plt.plot(x, vec)
        plt.yscale('log')
    else:
        mat = Gen.read_matrix(filename, create_object=False)
        """if i < no_plots:
            mplt.plot_2d(system_input.get_number_cells(), system_input.get_length(), mat, title=str(i))
        else:
            mplt.plot_2d(system_input.get_number_cells(), system_input.get_length(), mat, title=str(42))
        """
        mplt.plot_2d(N, 1, mat)

plt.show()
