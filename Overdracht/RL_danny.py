# (M xi,D)

rhs_phi_adj = np.zeros(L_no_dof)

for ordinate in range (no_ordinates):
  omega = omegas[ordinate]
  for j in range(nj):
    if omega[0] > 0.0:
      phi_index = dof(ordinate,ni-1,j,nj,nj)
      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] + dy * weights[ordinate] * omega[0] * det_right[j]
    if omega[0] < 0.0:
      phi_index = dof(ordinate,0,j,nj,nj)
      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] - dy * weights[ordinate] * omega[0] * det_left[j]
  for i in range(ni):
    if omega[1] > 0.0:
      phi_index = dof(ordinate,i,nj-1,nj,nj)
      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] + dx * weights[ordinate] * omega[1] * det_top[i]
    if omega[1] < 0.0:
      phi_index = dof(ordinate,i,0,nj,nj)
      rhs_phi_adj[phi_index] = rhs_phi_adj[phi_index] - dx * weights[ordinate] * omega[1] * det_bottom[i]
