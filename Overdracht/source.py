import numpy as np


class Source:
    max_source_value = 11

    # Initialize source
    def __init__(self, source_type, n_cells, l, center: float = 0.5, width: float = 1):
        self.type = source_type
        self.n_cells = n_cells
        self.length = l
        self.center = center
        self.width = width
        self.delta_x = self.length/self.n_cells
        self.source = np.zeros((2*n_cells))  # Source depends on both angle and location
        self.set_type()

    def get_source(self, i: int = None, half: int = None):
        if half == 1:
            if i is not None:
                return self.source[i]
            else:
                return self.source[0:self.n_cells]
        elif half == 2:
            if i is not None:
                return self.source[i+self.n_cells]
            else:
                return self.source[self.n_cells:(2*self.n_cells)]
        else:
            return self.source

    def set_type(self):
        if self.type == 0:
            self.ones()
        elif self.type == 1:
            self.delta_peak_isotropic()
        elif self.type == 2:
            self.delta_peak_mono()
        elif self.type == 3:
            self.block()
        elif self.type == 4:
            self.block_mono()
        elif self.type == 5:
            self.triangle()
        elif self.type == 6:
            self.left_border()
        elif self.type == 7:
            self.right_border()
        elif self.type == 8:
            self.left_border()
            self.right_border()
        elif self.type == 9:
            self.linearly_increasing()
        elif self.type == 10:
            self.two_blocks()
        elif self.type == 11:
            self.two_deltas()

    def ones(self):
        self.source[:] = np.ones(self.n_cells*2)

    def delta_peak_isotropic(self):
        for i in range(self.n_cells):
            if i == int(round(self.n_cells*self.center)):  # source is a delta peak at the center of the domain.
                self.source[i] = 1  # /self.delta_x
                self.source[i+self.n_cells] = 1  # /self.delta_x

    def delta_peak_mono(self):
        for i in range(self.n_cells):
            if i == int(round(self.n_cells*self.center)):
                self.source[i] = 1  # /self.delta_x

    def block(self):
        for i in range(self.n_cells):
            if abs(i - self.center*self.n_cells) < self.width*self.n_cells/2:
                self.source[i] = 1
                self.source[i+self.n_cells] = 1

    def block_mono(self):
        for i in range(self.n_cells):
            if abs(i - self.center*self.n_cells) < self.width*self.n_cells/2:
                self.source[i] = 1

    def triangle(self):
        for i in range(self.n_cells):
            if abs(i - self.center*self.n_cells) < self.width*self.n_cells/2:
                if i - self.center*self.n_cells < 0:
                    self.source[i] = 2*((i/self.n_cells - self.center + self.width)/self.width) - 1
                    self.source[i+self.n_cells] = 2*((i/self.n_cells - self.center + self.width)/self.width) - 1
                else:
                    self.source[i] = 2*((- i/self.n_cells + self.center + self.width)/self.width) - 1
                    self.source[i+self.n_cells] = 2*((- i/self.n_cells + self.center + self.width)/self.width) - 1

    def left_border(self):
        self.source[0] = 1

    def right_border(self):
        self.source[2*self.n_cells - 1] = 1

    def linearly_increasing(self):
        for i in range(self.n_cells):
            self.source[i] = i/self.n_cells

    # This function calls two blocks at the borders of the domain with width self.width
    def two_blocks(self):
        for i in range(self.n_cells):
            if i < self.width*self.n_cells:
                self.source[i] = 1
                self.source[i+self.n_cells] =1
            if i > self.center*self.n_cells:
                self.source[i] = 1
                self.source[i+self.n_cells] = 1

    # Creates two delta peaks at the locations self.center and self.width  ##cheaterrrr
    def two_deltas(self):
        i = int(round(self.center*self.n_cells))
        j = int(round(self.width*self.n_cells))
        self.source[i] = 1
        self.source[i+self.n_cells] = 1
        self.source[j] = 1
        self.source[j+self.n_cells] = 1

    def multiply(self, n: int):
        self.source *= n

    @staticmethod
    def get_max_source_value():
        return Source.max_source_value

    @staticmethod
    def initialize_center():
        center = None
        center_defined = False
        while not center_defined:
            try:
                center = float(input('Please enter a value between 0 and 1 for the center of the source\n'))
                if center <= 0 or center >= 1:
                    raise ValueError
                else:
                    center_defined = True
            except ValueError:
                print('You did not enter a value between 0 and 1')
        return center

    @staticmethod
    def initialize_width(center):
        width = None
        width_defined = False
        max_width = 2*min(center, 1 - center)
        output_string = 'enter a value between 0 and ' + str(max_width) + ' for the width of the source'
        while not width_defined:
            try:
                width = float(input('Please ' + output_string + '\n'))
                if width < 0 or width > max_width:
                    raise ValueError
                else:
                    width_defined = True
            except ValueError:
                print('You did not ' + output_string)
        return width

    @staticmethod
    def initialize_source(n_cells, length, name_of_input: str):
        source_type_information = 'Please enter the desired type of the ' + name_of_input + ':\n\
            0: Uniform distribution over the grid\n\
            1: Isotropic delta peak\n\
            2: Delta peak in one direction\n\
            3: Block function width specified width\n\
            4: Block function in single direction\n\
            5: Triangle shaped function\n\
            6: Border source left\n\
            7: Border source right\n\
            8: Border source on both sides\n\
            9: Linearly increasing source\n\
            10: Two blocks at the boundaries\n\
            default: empty source\n'
        source_type = None
        source_type_defined = False
        while not source_type_defined:
            try:
                source_type = int(input(source_type_information))
                if Source.get_max_source_value() < source_type or source_type < 0:
                    raise ValueError
                else:
                    source_type_defined = True
            except ValueError:
                print('You did not enter a number between 0 and ' + str(Source.get_max_source_value()))
        if source_type not in (0, 6, 7, 8, 9):
            center = Source.initialize_center()
            if source_type > 2:
                width = Source.initialize_width(center)
                return Source(source_type, n_cells, length, center, width)
            else:
                return Source(source_type, n_cells, length, center)
        else:
            return Source(source_type, n_cells, length)
        # source(source_type, number of cells in domain, number of mu discretizations, [center], [width])
