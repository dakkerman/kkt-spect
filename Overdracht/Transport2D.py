import numpy as np
import KKT_shape as Sh
from math import sqrt, acos
from scipy import sparse
from scipy.sparse import linalg as spalg
# import General_Functions as Gen


def calculate_phi(n, delta_x, cell_size, s_type: int, sigma_a: Sh.Grid2D, sigma_s: Sh.Grid2D, src: Sh.Grid2D, print_output=True):
    if print_output:
        print("Calculating transport matrix.")
    no_cells = n**2  # The amount of cells in a 2-dimensional grid
    directions = Sh.get_number_directions(2, s_type)  # The number of direction vectors that are discretized
    size = no_cells*directions  # The size that mat needs to be to contain all cells and all directions
    mat = sparse.lil_matrix((size, size))  # Initialize the matrix
    vec = np.zeros(size)  # Initialize the right-hand side vector
    direction_vectors = Sh.initialize_directions(2, s_type)
    normal_vectors = Sh.initialize_normals(2)
    ###
    for k in range(directions):  # Iterate over all directions
        weight = direction_vectors[k].get_weight()
        for j in range(n):  # Iterate over all columns
            for i in range(n):  # Iterate over all rows
                index = Sh.get_vector_index(n, i, j, direction=k, no_dim=2)  # The index of this cell and direction in the vector notation
                space_index = Sh.get_vector_index(n, i, j, no_dim=2)  # Index for all isotropic quantities (cross sections
                ###
                # Calculate the source term
                vec[index] = weight*np.pi*cell_size*src.get(i, j)
                ###
                # Flow into and from the cell
                for p in range(4):
                    flow = weight*np.pi*delta_x*np.dot(normal_vectors[p].get_normalized(),
                                                direction_vectors[k].get_normalized())
                    if flow > 0:
                        mat[index, index] += flow
                    else:
                        if normal_vectors[p].get_vector()[0] != 0:
                            neighbour = normal_vectors[p].get_vector()[0]
                            if 0 <= i + neighbour < n:  # Check if it is a border cell
                                mat[index, index + neighbour] += flow  # Incoming flow (row)
                        elif normal_vectors[p].get_vector()[1] != 0:
                            neighbour = normal_vectors[p].get_vector()[1]
                            if 0 <= j + neighbour < n:  # Check if it is a border cell
                                mat[index, index + neighbour*n] += flow  # Incoming flow (row)
                        else:
                            raise ValueError
                ###
                # Calculate scatter and absorption
                mat[index, index] += weight*np.pi*cell_size*sigma_s.get(i, j)  # Out scatter
                mat[index, index] += weight*np.pi*cell_size*sigma_a.get(i, j)  # Absorption
                for p in range(directions):  # Calculate in scatter from all four directions
                    scatter_weight = direction_vectors[p].get_weight()
                    #mat[index, space_index + p*no_cells] -= scatter_weight*weight*np.pi*cell_size*sigma_s.get(i,j)/4 
                    mat[index, space_index + p*no_cells] -= weight*np.pi*cell_size*sigma_s.get(i,j)/directions  # The line above SHOULD work, but this one DOES work....
    if print_output:
        print('Solving the transport matrix to obtain the flux')
    mat = mat.tocsr()
    raw_phi = spalg.spsolve(mat, vec)
    phi = Sh.Flux2D(raw_phi, s_type)
    ###
    # Check whether the balance between production, absorption and leakage holds
    if not calculate_balance(delta_x, cell_size, phi, src, sigma_a, print_output):
        raise ValueError('The balance does not hold')
    return mat, phi


def calculate_phi_higher_order(n, delta_x, cell_size, s_type: int, sigma_a: Sh.Grid2D, sigma_s: Sh.Grid2D, src: Sh.Grid2D, print_output=True):
    no_cells = n**2
    higher_order = 12
    phi_16 = calculate_phi(n, delta_x, cell_size, higher_order, sigma_a, sigma_s, src, print_output)[1].get_vector()
    a = calculate_phi(n, delta_x, cell_size, s_type, sigma_a, sigma_s, src, print_output)[0]
    dirs_16 = Sh.initialize_directions(2, higher_order)
    dirs = Sh.initialize_directions(2, s_type)
    no_dirs = Sh.get_number_directions(2, s_type)
    phi = np.zeros(no_dirs*no_cells)
    for l in range(no_dirs):
        min_dist = np.zeros(2) 
        best_k = np.zeros(2, dtype=int)
        for k in range(Sh.get_number_directions(2, higher_order)):
            dist = np.dot(dirs_16[k].get_normalized(), dirs[l].get_normalized())
            if dist > min_dist[0]:  # This direction is the best one yet
                min_dist[1] = min_dist[0]  # Replace the second best with the previous best
                min_dist[0] = dist
                best_k[1] = best_k[0]  # Replace the second best with the previous best
                best_k[0] = k  
            elif dist > min_dist[1]:  # This direction is the second best
                min_dist[1] = dist
                best_k[1] = k
        theta1 = acos(np.dot(dirs[l].get_normalized(), dirs_16[best_k[0]].get_normalized()))
        theta2 = acos(np.dot(dirs[l].get_normalized(), dirs_16[best_k[1]].get_normalized()))
        for i in range(no_cells):
            phi[i+l*no_cells] = (theta2*phi_16[i + best_k[0]*no_cells] + theta1*phi_16[i + best_k[1]*no_cells])/(theta1 + theta2)
    return a, Sh.Flux2D(phi, s_type)




def calculate_adjoint(n, delta_x, cell_size, s_type, sigma_a: Sh.Grid2D, sigma_s: Sh.Grid2D):
    print('Calculating adjoint')
    no_cells = n**2
    directions = Sh.get_number_directions(2, s_type)
    size = no_cells * directions
    mat = np.zeros((size, size))
    flow_factor = np.pi * delta_x / sqrt(3)
    for k in range(directions):
        direction_vector = Sh.direction_vector(k)
        for j in range(n):
            for i in range(n):
                index = Sh.get_vector_index(n, i, j, k)
                space_index = Sh.get_vector_index(n, i, j)
                # Flow
                mat[index, index] = 2*flow_factor
                if 0 <= i + direction_vector[0] < n-1:
                    mat[index, index + direction_vector[0]] -= flow_factor
                if 0 <= j + direction_vector[1] < n:
                    mat[index, index + direction_vector[1] * n] -= flow_factor
                # Calculate out scatter and absorption
                mat[index, index] += np.pi*cell_size*sigma_s.get(i, j) + np.pi*cell_size*sigma_a.get(i, j)
                # Calculate in scatter from all four directions
                for p in range(directions):
                    mat[index, space_index + p*no_cells] -= np.pi*cell_size*sigma_s.get(i, j)/directions
    return mat


def calculate_balance(delta_x, cell_size, phi: Sh.Flux2D, src: Sh.Grid2D, sx: Sh.Grid2D, print_output):
    src_sum = src.calculate_total_value(cell_size)
    phi_out = phi.calculate_outgoing_flux(delta_x)
    absorption = np.multiply(phi.scalar_flux(False), sx.get_matrix())
    absorption = Sh.Grid2D.initialize_from_matrix(absorption)
    absorption_sum = absorption.calculate_total_value(cell_size, integrated_over_all_directions=False)
    if print_output:
        print('Checking if the balance of production, absorption and leakage approximates zero')
        print('Total production is    ' + str(src_sum))
        print('Total outgoing flux is ' + str(phi_out))
        print('Total absorption is    ' + str(absorption_sum))
    balance = src_sum - phi_out - absorption_sum
    return -1e-6 < balance < 1e-6
