import numpy as np
from math import sqrt
import scipy.sparse as sp
import warnings


######################################################################################
class Grid:
    def __init__(self, vec: np.ndarray, s_type=0):
        self.vec = vec
        self.s_type = s_type

    def get_vector(self):
        return self.vec

    def get_repeated_vector(self, no_d):
        v = np.zeros(no_d*self.n)
        for i in range(no_d):
            v[i*self.n:(i+1)*self.n] = self.vec
        return v

    def get_weighted_vector(self, s_type, n, no_dim):
        direction_vectors = initialize_directions(2, s_type)
        no_directions = get_number_directions(no_dim, s_type)
        vector = np.zeros(n*no_directions)
        for k in range(no_directions):  # Iterate over all directions
            weight = direction_vectors[k].get_weight()
            vector[k*n:(k+1)*n] = weight*self.vec
        return vector


class Grid2D(Grid):
    def __init__(self, vec: np.ndarray, s_type=0):
        """
        Initialize a 2-dimensional grid from a vector
        :param vec: vector that contains the information
        """
        super().__init__(vec, s_type)
        self.directions = get_number_directions(2, self.s_type)
        self.n = int(round(vec.shape[0]/self.directions))
        self.l = int(round(sqrt(self.n)))

    def get(self, i, j=None, k=0):
        """
        Get a single value from the grid with matrix notation
        :param i: row #
        :param j: column #
        :param k: direction indicator ([1, 1], [-1, 1], [-1, -1], [1, -1])
        :return: value at grid point
        """
        if i >= self.l or j >= self.l or k >= self.directions:
            print('i = ' + str(i) + ', length = ' + str(self.l))
            print('j = ' + str(j) + ', length = ' + str(self.l))
            print('k = ' + str(k) + ', number of directions = ' + str(self.directions))
            raise ValueError
        if j is None:
            return self.vec[i]
        return self.vec[get_vector_index(self.l, i, j, k)]

    def get_length(self):
        return self.l

    def get_matrix(self, direction=None):
        if self.directions == 1:
            mat = np.zeros((self.l, self.l))
            for i in range(self.l):
                mat[i, :] = self.vec[i*self.l:(i+1)*self.l]
        elif direction is None:
            mat = np.zeros((4*self.l, 4*self.l, self.directions))
            for k in range(self.directions):
                for i in range(self.l):
                    for j in range(self.l):
                        mat[i, j, k] = self.get(i, j, k)
        else:
            mat = np.zeros((self.l, self.l))
            for i in range(self.l):
                mat[i, :] = self.vec[self.n*direction + i*self.l: self.n*direction + (i+1)*self.l]
        return mat

    def calculate_total_value(self, cell_size, integrated_over_all_directions=True):
        source_sum = 0
        f = np.pi * 4
        if not integrated_over_all_directions:
            f /= self.directions
        for i in range(self.l):
            for j in range(self.l):
                source_sum += f * cell_size * self.get(i, j)
        return source_sum

    @staticmethod
    def get_previous_cell(direction, n, i, j):
        m = n**2
        if direction == 0:
            if j+n < m:
                if i > 0:
                    return [i-1, j+n]
                else:
                    return [None, j+n]
            elif i > 0:
                return [i-1, None]

        elif direction == 1:
            return [i-1, j-n]
        elif direction == 2:
            return [i+1, j-n]
        elif direction == 3:
            return [i+1, j+n]

    @staticmethod
    def initialize_from_matrix(mat: np.ndarray, n=None):
        if n is None:
            n = mat.shape[0]
        vec = np.zeros(n**2)
        for i in range(n):
            vec[i*n:(i+1)*n] = mat[i, :]
        return Grid2D(vec)

    @staticmethod
    def initialize_center(n):
        """
        Function that will initialize a 2D Grid that is ones at a square in the center (area = 1/4) and zero elsewhere
        :param n: int- Length of the grid
        :return: Grid2D
        """
        mat = np.zeros((n, n))
        if n > 3:
            for i in range(n):
                if n / 4 <= i < 3 * n / 4:
                    boundary = int(round(n / 4))
                    mat[i, boundary:3 * boundary] = np.ones(2 * boundary)
        elif n == 3:
            mat[1, 1] = 1
        return Grid2D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_border(n: int):
        """
        Function that will initialize a 2D Grid that is ones at the border and zero everywhere else
        :param n: int- Length of the grid
        :return: Grid2D
        """
        mat = (1 / sqrt(3)) * np.pi * np.ones((n, n))
        mat[1:(n - 1), 1:(n - 1)] = np.zeros((n - 2, n - 2))
        return Grid2D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_full(n: int):
        """
        Function that will initialize a 2D Grid that is all ones
        :param n: int - Length of the grid
        :return: Grid2D
        """
        mat = np.ones((n, n))
        return Grid2D.initialize_from_matrix(mat, n)


    @staticmethod
    def initialize_peaks(n: int):
        mat = np.zeros((n, n))
        coordinate = int(round(n/4))
        mat[coordinate, coordinate] = 1
        mat[3*coordinate, coordinate] = 1
        mat[3*coordinate, 3*coordinate] = 1
        mat[coordinate, 3*coordinate] = 1
        return Grid2D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_dummy_16(n: int):
        mat = np.zeros((n, n))
        mat[2, 2] = 1
        mat[4:6, 4:6] = np.ones((2, 2))
        mat[10:14, 10:14] = np.ones((4, 4))
        mat[7:10, 4] = np.ones(3)
        mat[8, 3:6] = np.ones(3)
        mat[4, 10:15] = np.ones(5)
        mat[2:7, 12] = np.ones(5)
        return Grid2D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_dummy_32(n: int):
        mat = np.zeros((n, n))
        mat[2, 2] = 1
        mat[4:6, 4:6] = .5*np.ones((2, 2))
        mat[10:14, 10:14] = np.ones((4, 4))
        mat[7:10, 4] = np.ones(3)
        mat[8, 3:6] = np.ones(3)
        mat[4, 10:15] = np.ones(5)
        mat[2:7, 12] = np.ones(5)
        mat[16:20, 2:18] = np.ones((4, 16))
        mat[12:16, 14:18] = np.ones((4, 4))
        mat[25:30, 25:30] = np.ones((5, 5))
        mat[26:29, 26:29] = np.zeros((3, 3))
        mat[26, 2] = 1
        mat[26, 5] = .5
        mat[29, 3] = .5
        mat[30, 7] = 1
        mat[3:11, 16:24] = np.ones((8, 8))
        mat[6:8, 19:21] = np.zeros((2, 2)) 
        mat[27, 20:24] = np.ones(4)
        return Grid2D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_cactus(n: int):
        mat = np.zeros((n, n))
        mat[0:28, 14:19] = np.ones((28, 5))
        mat[10:14, 3:14] = np.ones((4, 11))
        mat[14:20, 3:7] = np.ones((6, 4))
        mat[13:17, 19:28] = np.ones((4, 9))
        mat[17:22, 24:28] = np.ones((5, 4))
        mat[20:22, 10:14] = np.ones((2, 4))
        mat[22:24, 10:12] = np.ones((2, 2))
        return Grid2D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_curve(n: int):
        mat = np.zeros((n, n))
        for i in range(3, 8):
            for j in range(3, 8):
                mat[i, j] = i + j
                mat[i, n-j-1] = i+j
                mat[n-i-1, j] = i+j
                mat[n-i-1, n-j-1] = i+j
        return Grid2D.initialize_from_matrix(mat, n)
#############################################################################
class Grid3D(Grid):
    def __init__(self, vec: np.ndarray, s_type=0):
        """
        Initialize a 3-dimensional grid from a vector
        :param vec: vector that contains the information
        """
        super().__init__(vec, s_type)
        self.directions = get_number_directions(3, self.s_type)
        self.n = int(round(vec.shape[0]/self.directions))
        self.l = int(round(self.n**(1/3)))

    def get(self, i, j, l, k=0):
        """
        Get a single value from the grid with matrix notation
        :param i: row #
        :param j: column #
        :param l: z-coordinate #
        :param k: direction indicator ([1, 1], [-1, 1], [-1, -1], [1, -1])
        :return: value at grid point
        """
        if i >= self.l or j >= self.l or k >= self.directions:
            print('i = ' + str(i) + ', length = ' + str(self.l))
            print('j = ' + str(j) + ', length = ' + str(self.l))
            print('l = ' + str(l) + ', length = ' + str(self.l))
            print('k = ' + str(k) + ', number of directions = ' + str(self.directions))
            raise ValueError
        if j is None:
            return self.vec[i]
        return self.vec[get_vector_index(self.l, i, j, l=l, direction=k, no_dim=3)]

    def get_length(self):
        return self.l

    def get_matrix(self, direction=0):
        mat = np.zeros((self.l, self.l, self.l))
        for i in range(self.l):
            for j in range(self.l):
                for z in range(self.l):
                    mat[i, j, z] = self.get(i, j, l=z, k=direction)
        return mat

    def calculate_total_value(self, cell_size, integrated_over_all_directions=True):
        source_sum = 0
        f = np.pi * 4
        if not integrated_over_all_directions:
            f /= self.directions
        for i in range(self.l):
            for j in range(self.l):
                for l in range(self.l):
                    source_sum += f * cell_size * self.get(i, j, l=l)
        return source_sum

    @staticmethod
    def get_previous_cell(direction, n, i, j, l):
        if direction == 0:
            return [i-1, j+n] 
        elif direction == 1:
            return [i-1, j-n]
        elif direction == 2:
            return [i+1, j-n]
        elif direction == 3:
            return [i+1, j+n]
        elif direction == 4:
            return [i-1, j+n]
        elif direction == 5:
            return [i-1, j-n]
        elif direction == 6:
            return [i+1, j+n]
        elif direction == 7:
            return [i+1, j+n]

    @staticmethod
    def initialize_from_matrix(mat: np.ndarray, n=None):
        if n is None:
            n = mat.shape[0]
        vec = np.zeros(n**3)
        for i in range(n):
            for j in range(n):
                vec[i*n+j*n**2:(i+1)*n+j*n**2] = mat[i, :, j]
        return Grid3D(vec)

    @staticmethod
    def initialize_center(n, width):
        """
        Function that will initialize a 3D Grid that is ones at a square in the center (area = 1/4) and zero elsewhere
        :param n: int- Length of the grid
        :return: Grid3D
        """
        mat = np.zeros((n, n, n))
        bw = int((n-width)/2)
        mat[bw:n-bw, bw:n-bw, bw:n-bw] = np.ones((width, width, width))
        return Grid3D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_border(n: int):
        """
        Function that will initialize a 2D Grid that is ones at the border and zero everywhere else
        :param n: int- Length of the grid
        :return: Grid2D
        """
        mat = (1 / sqrt(3)) * np.pi * np.ones((n, n, n))
        mat[1:(n - 1), 1:(n - 1), 1:(n-1)] = np.zeros((n - 2, n - 2, n-2))
        return Grid3D.initialize_from_matrix(mat, n)

    @staticmethod
    def initialize_full(n: int):
        """
        Function that will initialize a 2D Grid that is all ones
        :param n: int - Length of the grid
        :return: Grid2D
        """
        mat = np.ones((n, n, n))
        return Grid3D.initialize_from_matrix(mat, n)


    @staticmethod
    def initialize_dummy_16(n: int):
        """
        """
        mat = np.zeros((n, n, n))
        mat[2, 2, :] = np.ones(n)
        mat[4:6, 4:6, :] = np.ones((2, 2, n))
        mat[10:14, 10:14, :] = np.ones((4, 4, n))
        mat[7:10, 4, :] = np.ones((3, n))
        mat[8, 3:6, :] = np.ones((3, n))
        return Grid3D.initialize_from_matrix(mat, n)

################################################################################################
class Flux2D(Grid2D):
    def __init__(self, vec: np.ndarray, s_type):
        super().__init__(vec, s_type)

    def get_single_flux(self, direction=0):
        if direction >= self.directions:
            raise ValueError
        vec = self.vec[self.n*direction:self.n*(direction+1)]
        mat = np.zeros((self.l, self.l))
        for i in range(self.l):
            for j in range(self.l):
                mat[i, j] = vec[j + self.l*i]
        return mat

    def calculate_outgoing_flux(self, delta_x):
        phi_border = 0
        direction_vectors = initialize_directions(2, self.s_type)
        normal_vectors = initialize_normals(2)
        for k in range(self.directions):
            phi_k = self.get_single_flux(direction=k)
            for p in range(4):
                flow = (4/self.directions) * np.pi * delta_x * np.dot(direction_vectors[k].get_normalized(),
                                                                      normal_vectors[p].get_normalized())
                if flow > 0:
                    for i in range(self.l):
                        if p == 0:
                            phi_border += flow*phi_k[i, self.l-1]
                        elif p == 1:
                            phi_border += flow*phi_k[self.l-1, i]
                        elif p == 2:
                            phi_border += flow*phi_k[i, 0]
                        elif p == 3:
                            phi_border += flow*phi_k[0, i]
        return phi_border

    def scalar_flux(self, integrated=True):
        phi = 0
        if integrated:
            f = 4*np.pi/self.directions
        else:
            f = 1/self.directions
        for k in range(self.directions):
            phi += f * self.get_single_flux(direction=k)
        return phi

    @staticmethod
    def initialize_collimated_detector(n: int, dimensions: int, s_type: int):
        direction_vectors = initialize_directions(dimensions, s_type)
        directions = get_number_directions(dimensions, s_type)
        normal_vectors = initialize_normals(2)
        no_entries = n**dimensions * directions
        sigma_d = sp.lil_matrix((no_entries, no_entries))
        for i in range(n):
            for j in range(2*dimensions):
                normal_vector = normal_vectors[j].get_normalized()
                if j == 0:
                    indices = np.array([0, i])
                elif j == 1:
                    indices = np.array([i, n-1])
                elif j == 2:
                    indices = np.array([n-1, i])
                elif j == 3:
                    indices = np.array([i, 0])
                min_dist = np.zeros(2) 
                best_k = np.zeros(2)
                for k in range(directions):
                    dist = np.dot(normal_vector, direction_vectors[k].get_normalized())
                    if dist > min_dist[0]:  # This direction is the best one yet
                        min_dist[1] = min_dist[0]  # Replace the second best with the previous best
                        min_dist[0] = dist
                        best_k[1] = best_k[0]  # Replace the second best with the previous best
                        best_k[0] = k  
                    elif dist > min_dist[1]:  # This direction is the second best
                        min_dist[1] = dist
                        best_k[1] = k
                total_distance = 0
                difference = np.zeros(2)
                index = np.zeros(2)
                if True:#i > 0 and i < n-1:
                    for p in range(2):
                        difference[p] = np.linalg.norm(normal_vector - direction_vectors[int(best_k[p])].get_normalized())
                        total_distance += difference[p]
                        index[p] = int(get_vector_index(n, indices[0], indices[1], best_k[p], no_dim=2))
                    for p in range(2):
                        weight = difference[p]/total_distance
                        sigma_d[index[p], index[p]] += weight**2  #The weights are squared because effectively for the application of sigma_d, the elements must be squarded. 
                        sigma_d[index[p-1], index[p]] += weight**2  #See above comment
        return sigma_d


    @staticmethod
    def initialize_collimated_detector_fout(n: int, dimensions: int, s_type: int):
        direction_vectors = initialize_directions(dimensions, s_type)
        directions = get_number_directions(dimensions, s_type)
        normal_vectors = initialize_normals(2)
        no_entries = n**dimensions * directions
        sigma_d = sp.lil_matrix((no_entries, no_entries))
        for i in range(n):
            for j in range(2*dimensions):
                normal_vector = normal_vectors[j].get_normalized()
                """if j == 0:
                    indices = np.array([i, n-1])
                elif j == 1:
                    indices = np.array([n-1, i])
                elif j == 2:
                    indices = np.array([i, 0])
                elif j == 3:
                    indices = np.array([0, i])"""
                if j == 0:
                    indices = np.array([n-1, i])
                elif j == 1:
                    indices = np.array([i, n-1])
                elif j == 2:
                    indices = np.array([0, i])
                elif j == 3:
                    indices = np.array([i, 0])
                min_dist = np.zeros(2) 
                best_k = np.zeros(2)
                for k in range(directions):
                    dist = np.dot(normal_vector, direction_vectors[k].get_normalized())
                    if dist > min_dist[0]:  # This direction is the best one yet
                        min_dist[1] = min_dist[0]  # Replace the second best with the previous best
                        min_dist[0] = dist
                        best_k[1] = best_k[0]  # Replace the second best with the previous best
                        best_k[0] = k  
                    elif dist > min_dist[1]:  # This direction is the second best
                        min_dist[1] = dist
                        best_k[1] = k
                total_distance = 0
                difference = np.zeros(2)
                index = np.zeros(2)
                for p in range(2):
                    difference[p] = np.linalg.norm(normal_vector - direction_vectors[int(best_k[p])].get_normalized())
                    total_distance += difference[p]
                    index[p] = int(get_vector_index(n, indices[0], indices[1], best_k[p], no_dim=2))
                for p in range(2):
                    weight = difference[p]/total_distance
                    sigma_d[index[p], index[p]] += weight
        return sigma_d

    @staticmethod
    def initialize_collimated_detector_scheef(n: int, dimensions: int, s_type: int):
        direction_vectors = initialize_directions(dimensions, s_type)
        directions = get_number_directions(dimensions, s_type)
        normal_vectors = initialize_normals(2)
        no_entries = n**dimensions * directions
        sigma_d = sp.lil_matrix((2*dimensions*n, no_entries))
        for i in range(n):
            for j in range(2*dimensions):
                normal_vector = normal_vectors[j].get_normalized()
                if j == 0:
                    indices = np.array([n-1, i])
                elif j == 1:
                    indices = np.array([i, n-1])
                elif j == 2:
                    indices = np.array([0, i])
                elif j == 3:
                    indices = np.array([i, 0])
                min_dist = np.zeros(2) 
                best_k = np.zeros(2)
                for k in range(directions):
                    dist = np.dot(normal_vector, direction_vectors[k].get_normalized())
                    if dist > min_dist[0]:  # This direction is the best one yet
                        min_dist[1] = min_dist[0]  # Replace the second best with the previous best
                        min_dist[0] = dist
                        best_k[1] = best_k[0]  # Replace the second best with the previous best
                        best_k[0] = k  
                    elif dist > min_dist[1]:  # This direction is the second best
                        min_dist[1] = dist
                        best_k[1] = k
                total_distance = 0
                difference = np.zeros(2)
                index = np.zeros(2)
                for p in range(2):
                    difference[p] = np.linalg.norm(normal_vector - direction_vectors[int(best_k[p])].get_normalized())
                    total_distance += difference[p]
                    index[p] = int(get_vector_index(n, indices[0], indices[1], best_k[p], no_dim=2))
                for p in range(2):
                    weight = difference[p]/total_distance
                    sigma_d[i+j*n, index[p]] += weight#**2  #The weights are squared because effectively for the application of sigma_d, the elements must be squarded. 
                    sigma_d[i+j*n, index[p-1]] += weight#**2  #See above comment
        return sigma_d

    @staticmethod
    def initialize_current_detector_scheef(n: int, no_dim: int, s_type: int):
        direction_vectors = initialize_directions(no_dim, s_type)
        directions = get_number_directions(no_dim, s_type)
        normal_vectors = initialize_normals(no_dim)
        sz = n**no_dim * directions
        sigma_d = sp.lil_matrix((2*no_dim*n, sz))
        for i in range(n):
            for j in range(2*no_dim):
                normal_vector = normal_vectors[j].get_normalized()
                if j == 0:
                    indices = np.array([n-1, i])
                elif j == 1:
                    indices = np.array([i, n-1])
                elif j == 2:
                    indices = np.array([0, i])
                elif j == 3:
                    indices = np.array([i, 0])
                for k in range(directions):
                    weight = direction_vectors[k].get_weight()*np.dot(normal_vector, direction_vectors[k].get_vector())
                    if np.dot(direction_vectors[k].get_vector(), normal_vector) > 0:
                        index = get_vector_index(n, indices[0], indices[1], k, no_dim=no_dim)
                        sigma_d[i+j*n, index] += weight
        sigma_d = sigma_d.tocsr()
        return sigma_d

    @staticmethod
    def initialize_current_detector_fout(n: int, no_dim: int, s_type: int):
        direction_vectors = initialize_directions(no_dim, s_type)
        directions = get_number_directions(no_dim, s_type)
        normal_vectors = initialize_normals(no_dim)
        sz = n**no_dim * directions
        sigma_d = sp.lil_matrix((sz, sz))
        for i in range(n):
            for j in range(2*no_dim):
                normal_vector = normal_vectors[j].get_normalized()
                if j == 0:
                    indices = np.array([n-1, i])
                elif j == 1:
                    indices = np.array([i, n-1])
                elif j == 2:
                    indices = np.array([0, i])
                elif j == 3:
                    indices = np.array([i, 0])
                for k in range(directions):
                    weight = direction_vectors[k].get_weight()*np.dot(normal_vector, direction_vectors[k].get_vector())
                    if np.dot(direction_vectors[k].get_vector(), normal_vector) > 0:
                        index = get_vector_index(n, indices[0], indices[1], k, no_dim=no_dim)
                        sigma_d[index, index] = weight
        sigma_d = sigma_d.tocsr()
        return sigma_d

    @staticmethod
    def initialize_single_flux_detector(n: int, no_dim: int, s_type: int):
        direction_vectors = initialize_directions(no_dim, s_type)
        directions = get_number_directions(no_dim, s_type)
        normal_vectors = initialize_normals(no_dim)
        sz = n**no_dim * directions
        sigma_d = sp.lil_matrix((sz, sz))
        for i in range(n):
            for j in range(2*no_dim):
                normal_vector = normal_vectors[j].get_normalized()
                if j == 0:
                    indices = np.array([n-1, i])
                elif j == 1:
                    indices = np.array([i, n-1])
                elif j == 2:
                    indices = np.array([0, i])
                elif j == 3:
                    indices = np.array([i, 0])
                min_dist = 0
                best_k = None
                for k in range(directions):
                    dist = np.dot(normal_vector, direction_vectors[k].get_normalized())
                    if dist > min_dist:  # This direction is the best one yet
                        min_dist = dist
                        best_k = k  
                index = get_vector_index(n, indices[0], indices[1], best_k, no_dim=no_dim)
                sigma_d[index, index] = 1
        return sigma_d.tocsr()
##################################################################################################################
class Flux3D(Grid3D):
    def __init__(self, vec: np.ndarray, s_type):
        super().__init__(vec, s_type)

    def get_single_slice(self, direction=0, z=0):
        if direction >= self.directions:
            raise ValueError
        vec = self.vec[self.n*direction + z*self.l**2:self.n*(direction+1) + z*self.l**2]
        mat = np.zeros((self.l, self.l))
        for i in range(self.l):
            for j in range(self.l):
                mat[i, j] = vec[j + self.l*i]
        return mat

    def get_single_flux(self, direction=0):
        if direction >= self.directions:
            raise ValueError
        mat = np.zeros((self.l, self.l, self.l))
        for i in range(self.l):
            for j in range(self.l):
                for z in range(self.l):
                    mat[i, j, z] = self.vec[get_vector_index(self.l, i, j, direction=direction, l=z, no_dim=3)]
        return mat

    def calculate_outgoing_flux(self, delta_x):
        face_size = delta_x**2
        phi_border = 0
        direction_vectors = initialize_directions(3, self.s_type)
        normal_vectors = initialize_normals(3)
        for k in range(self.directions):
            phi_k = self.get_single_flux(direction=k)
            for p in range(6):
                flow = (4/self.directions) * np.pi * face_size * np.dot(direction_vectors[k].get_normalized(),
                                                                      normal_vectors[p].get_normalized())
                if flow > 0:
                    for i in range(self.l):
                        for j in range(self.l):
                            if p == 0:
                                phi_border += 1*flow*phi_k[self.l-1, i, j]
                            elif p == 1:
                                phi_border += 1*flow*phi_k[i, self.l-1, j]
                            elif p == 2:
                                phi_border += 1*flow*phi_k[0, i, j]
                            elif p == 3:
                                phi_border += 1*flow*phi_k[i, 0, j]
                            elif p == 4:
                                phi_border += 1*flow*phi_k[i, j, self.l-1]
                            elif p == 5:
                                phi_border += 1*flow*phi_k[i, j, 0]
        return phi_border

    def scalar_flux(self, integrated=True):
        phi = 0
        if integrated:
            f = 4*np.pi/self.directions
        else:
            f = 1/self.directions
        for k in range(self.directions):
            phi += f * self.get_single_flux(direction=k)
        return phi

    @staticmethod
    def initialize_collimated_detector(n: int, dimensions: int, s_type: int):
        direction_vectors = initialize_directions(dimensions, s_type)
        directions = get_number_directions(dimensions, s_type)
        vec = np.zeros(n**2*directions) 
        normal_vectors = initialize_normals(2)
        for i in range(n):
            for j in range(2*dimensions):
                normal_vector = normal_vectors[j].get_normalized()
                """if j == 0:
                    indices = np.array([i, n-1])
                elif j == 1:
                    indices = np.array([n-1, i])
                elif j == 2:
                    indices = np.array([i, 0])
                elif j == 3:
                    indices = np.array([0, i])"""
                if j == 0:
                    indices = np.array([n-1, i])
                elif j == 1:
                    indices = np.array([i, n-1])
                elif j == 2:
                    indices = np.array([0, i])
                elif j == 3:
                    indices = np.array([i, 0])
                min_dist = np.zeros(2) 
                best_k = np.zeros(2)
                for k in range(directions):
                    dist = np.dot(normal_vector, direction_vectors[k].get_normalized())
                    if dist > min_dist[0]:  # This direction is the best one yet
                        min_dist[1] = min_dist[0]  # Replace the second best with the previous best
                        min_dist[0] = dist
                        best_k[1] = best_k[0]  # Replace the second best with the previous best
                        best_k[0] = k  
                    elif dist > min_dist[1]:  # This direction is the second best
                        min_dist[1] = dist
                        best_k[1] = k
                total_distance = 0
                difference = np.zeros(2)
                for p in range(2):
                    difference[p] = np.linalg.norm(normal_vector - direction_vectors[int(best_k[p])].get_normalized())
                    total_distance += difference[p]
                for p in range(2):
                    weight = difference[p]/total_distance
                    index = int(get_vector_index(n, indices[0], indices[1], best_k[p]))
                    vec[index] = weight
        return Grid2D(vec, s_type)

################################################################################################
class TwoDimensionalKktMatrix:
    def __init__(self, mat):
        self.n = int(round(mat.shape[0]/9))
        self.h = mat[0:5*self.n, 0:5*self.n]
        self.a = mat[5*self.n:9*self.n, self.n:5*self.n]

    def get_h(self):
        return self.h

    def get_a(self):
        return self.a

    def get_k(self):
        return self.h[self.n:5*self.n, self.n:5*self.n]


#########################################################################################
class KktVector:
    def __init__(self, n, cell_size, s_type: int, vec, no_dim):
        self.s_type = s_type
        self.directions = get_number_directions(2, s_type)
        self.n = n
        self.vec = vec
        self.phi_t = vec[(self.directions+1)*n:]
        # self.q = Grid2D(self.directions*vec[0:n]/(4*np.pi*cell_size))
        self.q = Grid2D(vec[0:n])
        if no_dim == 2:
            self.phi = Flux2D(vec[n:(self.directions+1) * n], s_type)
        elif no_dim == 3:
            self.phi = Flux3D(vec[n:(self.directions+1) * n], s_type)

    def get_vector(self):
        return self.vec

    def get_q(self, orientation='mat', direction=None):
        if orientation == 'mat':
            return self.q.get_matrix()
        else:
            return self.q.get_vector()

    def get_phi(self, direction=None):
        return self.phi

    def get_phi_t(self, direction=None):
        return self.phi_t

    def get_phi_scalar(self):
        return self.phi.scalar_flux()


#########################################################################################
class DirectionVector:
    def __init__(self, vec: np.ndarray, weight=None):
        self.length = np.linalg.norm(vec)
        self.normalized = vec/self.length
        self.vector = vec
        self.weight = weight

    def get_normalized(self):
        """
        This method returns the normalized direction vector
        :return: np.ndarray(2)
        """
        return self.normalized

    def get_vector(self):
        """
        This method returns the non-normalized direction vector
        :return: np.ndarray(2)
        """
        return self.vector


    def get_weight(self):
        """
        This method returns the weight of the direction vector
        :return: float
        """
        return self.weight


#########################################################################################

def get_matrix_index(i: int, n: int):
    """
    Takes a vector index and outputs a matrix index
    :param i: Vector index
    :param n: Length of the (n x n)-matrix
    :return: int
    """
    if i+1 > n**2:
        raise ValueError
    for j in range(n):
        if i-n >= 0:
            i -= n
        else:
            break
    return [j, i]


def get_vector_index(n, i, j=0, direction=0, l=0, no_dim=1):
    """
    Translates matrix indices to a vector index
    :param n: Number of elements in a column
    :param i: x index
    :param j: y index
    :param k: Direction index
    :return: int
    """
    return (n**no_dim)*direction + n**2*l + n*j + i


def initialize_directions(dimensions, s_type):
    if s_type == 2:
        directions = 1
    elif s_type == 4:
        directions = 3
    elif s_type == 6:
        directions = 6
    elif s_type == 8:
        directions = 10
    elif s_type == 12:
        directions = 21
    elif s_type == 16:
        directions = 36
    index = 0
    direction_vectors = []
    for octant in range(2**dimensions):
        if octant%4 == 0 or octant%4 == 3:  # 0, 2, 4, 6
            x_sign = 1
        else:
            x_sign = -1
        if octant%4 == 0 or octant%4 == 1:  # 0, 1, 4, 5
            y_sign = 1
        else:
            y_sign = -1
        if octant < 4:  # 0, 1, 2, 3
            z_sign = 1
        else:
            z_sign = -1
        for j in range(directions):
            try:
                direction_vectors.append(direction_vector(s_type, x_sign, y_sign, j, z_sign=z_sign))
            except ValueError:
                pass
    return direction_vectors


def direction_vector(s_type, x_sign, y_sign, i, z_sign=0):
    if s_type == 16:
        mu1 = .1389568
        mu2 = .3922893
        mu3 = .5370966
        mu4 = .6504264
        mu5 = .7467506
        mu6 = .8319966
        mu7 = .9092855
        mu8 = .9805009
        w1 = .0489872
        w2 = .0413296
        w3 = .0212326
        w4 = .0256207
        w5 = .0360486
        w6 = .0144589
        w7 = .0344958
        w8 = .0085179
        if i == 0:
            return DirectionVector(np.array([x_sign*mu8, y_sign*mu1, z_sign*mu1]), w1)
        elif i == 1:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu8, z_sign*mu1]), w1)
        elif i == 2:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu1, z_sign*mu8]), w1)
        elif i == 3:
            return DirectionVector(np.array([x_sign*mu7, y_sign*mu2, z_sign*mu1]), w2)
        elif i == 4:
            return DirectionVector(np.array([x_sign*mu7, y_sign*mu1, z_sign*mu2]), w2)
        elif i == 5:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu7, z_sign*mu1]), w2)
        elif i == 6:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu1, z_sign*mu7]), w2)
        elif i == 7:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu7, z_sign*mu2]), w2)
        elif i == 8:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu2, z_sign*mu7]), w2)
        elif i == 9:
            return DirectionVector(np.array([x_sign*mu6, y_sign*mu3, z_sign*mu1]), w3)
        elif i == 10:
            return DirectionVector(np.array([x_sign*mu6, y_sign*mu1, z_sign*mu3]), w3)
        elif i == 11:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu6, z_sign*mu1]), w3)
        elif i == 12:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu1, z_sign*mu6]), w3)
        elif i == 13:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu6, z_sign*mu3]), w3)
        elif i == 14:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu3, z_sign*mu6]), w3)
        elif i == 15:
            return DirectionVector(np.array([x_sign*mu5, y_sign*mu4, z_sign*mu1]), w4)
        elif i == 16:
            return DirectionVector(np.array([x_sign*mu5, y_sign*mu1, z_sign*mu4]), w4)
        elif i == 17:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu5, z_sign*mu1]), w4)
        elif i == 18:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu1, z_sign*mu5]), w4)
        elif i == 19:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu5, z_sign*mu4]), w4)
        elif i == 20:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu4, z_sign*mu5]), w4)
        elif i == 21:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu4, z_sign*mu2]), w5)
        elif i == 22:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu2, z_sign*mu4]), w5)
        elif i == 23:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu4, z_sign*mu4]), w5)
        elif i == 24:
            return DirectionVector(np.array([x_sign*mu5, y_sign*mu3, z_sign*mu2]), w6)
        elif i == 25:
            return DirectionVector(np.array([x_sign*mu5, y_sign*mu2, z_sign*mu3]), w6)
        elif i == 26:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu5, z_sign*mu2]), w6)
        elif i == 27:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu2, z_sign*mu5]), w6)
        elif i == 28:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu5, z_sign*mu3]), w6)
        elif i == 29:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu3, z_sign*mu5]), w6)
        elif i == 30:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu4, z_sign*mu2]), w7)
        elif i == 31:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu2, z_sign*mu4]), w7)
        elif i == 32:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu4, z_sign*mu4]), w7)
        elif i == 33:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu3, z_sign*mu4]), w8)
        elif i == 34:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu4, z_sign*mu3]), w8)
        elif i == 35:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu3, z_sign*mu4]), w8)
    elif s_type == 12:
        mu1 = .1672126
        mu2 = .4595476
        mu3 = .6280191
        mu4 = .7600210
        mu5 = .8722706
        mu6 = .9716377
        w1 = .0707626
        w2 = .0558811
        w3 = .0373377
        w4 = .0502819
        w5 = .0258513
        if i == 0:
            return DirectionVector(np.array([x_sign*mu6, y_sign*mu1, z_sign*mu1]), w1)
        elif i == 1:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu6, z_sign*mu1]), w1)
        elif i == 2:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu1, z_sign*mu6]), w1)
        elif i == 3:
            return DirectionVector(np.array([x_sign*mu5, y_sign*mu2, z_sign*mu1]), w2)
        elif i == 4:
            return DirectionVector(np.array([x_sign*mu5, y_sign*mu1, z_sign*mu2]), w2)
        elif i == 5:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu5, z_sign*mu1]), w2)
        elif i == 6:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu1, z_sign*mu5]), w2)
        elif i == 7:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu5, z_sign*mu2]), w2)
        elif i == 8:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu2, z_sign*mu5]), w2)
        elif i == 9:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu3, z_sign*mu1]), w3)
        elif i == 10:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu1, z_sign*mu3]), w3)
        elif i == 11:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu4, z_sign*mu1]), w3)
        elif i == 12:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu1, z_sign*mu4]), w3)
        elif i == 13:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu4, z_sign*mu3]), w3)
        elif i == 14:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu3, z_sign*mu4]), w3)
        elif i == 15:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu2, z_sign*mu2]), w4)
        elif i == 16:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu4, z_sign*mu2]), w4)
        elif i == 17:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu2, z_sign*mu4]), w4)
        elif i == 18:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu3, z_sign*mu2]), w5)
        elif i == 19:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu2, z_sign*mu3]), w5)
        elif i == 20:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu3, z_sign*mu3]), w5)
    elif s_type == 8:
        mu1 = .2182179
        mu2 = .5773503
        mu3 = .7867958
        mu4 = .9511897
        w1 = .1209877
        w2 = .0907407
        w3 = .0925926
        if i == 0:
            return DirectionVector(np.array([x_sign*mu4, y_sign*mu1, z_sign*mu1]), w1)
        elif i == 1:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu4, z_sign*mu1]), w1)
        elif i == 2:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu1, z_sign*mu4]), w1)
        elif i == 3:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu2, z_sign*mu1]), w2)
        elif i == 4:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu1, z_sign*mu2]), w2)
        elif i == 5:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu3, z_sign*mu1]), w2)
        elif i == 6:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu1, z_sign*mu3]), w2)
        elif i == 7:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu3, z_sign*mu2]), w2)
        elif i == 8:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu2, z_sign*mu3]), w2)
        elif i == 9:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu2, z_sign*mu2]), w3)
    elif s_type == 6:
        mu1 = .2666355
        mu2 = .6815076
        mu3 = .9261808
        w1 = .1761263
        w2 = .1572071
        if i == 0:
            return DirectionVector(np.array([x_sign*mu3, y_sign*mu1, z_sign*mu1]), w1)
        elif i == 1:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu3, z_sign*mu1]), w1)
        elif i == 2:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu1, z_sign*mu3]), w1)
        elif i == 3:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu2, z_sign*mu1]), w2)
        elif i == 4:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu1, z_sign*mu2]), w2)
        elif i == 5:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu2, z_sign*mu2]), w2)
    elif s_type == 4:
        mu1 = .3500212
        mu2 = .8688903
        w1 = .3333333
        if i == 0:
            return DirectionVector(np.array([x_sign*mu2, y_sign*mu1, z_sign*mu1]), w1)
        elif i == 1:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu2, z_sign*mu1]), w1)
        elif i == 2:
            return DirectionVector(np.array([x_sign*mu1, y_sign*mu1, z_sign*mu2]), w1)
    elif s_type == 2:
        mu1 = .5773503
        w1 = 1
        return DirectionVector(np.array([x_sign*mu1, y_sign*mu1, z_sign*mu1]), w1)


def normal_vector(k):
    """
    :param k: Normal vector selector
    :return: DirectionVector
    """
    if k == 0:
        return DirectionVector(np.array([1, 0, 0]))
    elif k == 1:
        return DirectionVector(np.array([0, 1, 0]))
    elif k == 2:
        return DirectionVector(np.array([-1, 0, 0]))
    elif k == 3:
        return DirectionVector(np.array([0, -1, 0]))
    elif k == 4:
        return DirectionVector(np.array([0, 0, 1]))
    elif k == 5:
        return DirectionVector(np.array([0, 0, -1]))
    else:
        raise ValueError('Too many directions demanded')


def initialize_normals(dimensions: int):
    """
    Initializes normal vectors for a cubic grid
    :param dimensions: int - number of dimensions of the situation
    :return: list[DirectionVector]
    """
    normals = []
    for k in range(2*dimensions):
        try:
            normals.append(normal_vector(k))
        except ValueError:
            pass
    return normals


def get_number_directions(dimensions: int, s_type: int):
    """
    :param dimensions: int - number of dimensions of the situation (1, 2, 3)
    :param s_type: int - direction vector type (so far 0, 2, 4, 6)
    :return: int
    """
    multiplier = 2**dimensions
    if s_type == 0:
        return 1
    elif s_type == 2:
        return 1*multiplier
    elif s_type == 4:
        return 3*multiplier
    elif s_type == 6:
        return 6*multiplier
    elif s_type == 8:
        return 10*multiplier
    elif s_type == 12:
        return 21*multiplier
    elif s_type == 16:
        return 36*multiplier
    else:
        raise ValueError('S-type ' + str(s_type) + ' not supported')
