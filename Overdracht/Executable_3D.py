import numpy as np
import General_Functions as Gen
import Transport3D as Tr
import KKT_solver as Sol
import KKT_shape as Sh
import Minres
import time
from scipy.sparse import linalg as spla
from scipy import sparse
#########################################################################################################################
# Preconditioning methods
def no_preco(vec):
    return vec

# Caluclate Cx
def c_product(vec):
    y = np.zeros(no_directions*no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(1+k)*no_cells] = -weight*np.pi*cell_size*vec
    return y

# Calculate CTx
def ct_product(vec):
    y = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y -= weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    return y 
        
# Calculate CCTx
def cct_product(vec):
    temp = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        temp += weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    y = np.zeros(no_cells*no_directions)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(k+1)*no_cells] += weight*np.pi*cell_size*temp
    return y
        
# MINRES callback function
def callback(vec):
    #print('Iteration number ' + str(callback_index) + ' \t Residual ' + '{:.2e}'.format(vec))
    residuals.append(vec)
    global callback_index
    callback_index += 1
    print(callback_index, vec)



########################################################################################################################
# Initialization of user input constants
N = 16  # Number of cells in one x-direction
L = 1  # Length of the system
alpha = 1E-6  # Regularization constant
absorption_xsec = .048  #.048 cm^-1 - For 140 keV gammas
scatter_xsec = .172 #.172 cm^-1 - For 140 keV gammas
s_type = 2  # Measure for the amount of direction discretizations. Currently choose between S2 (1) and S4 (3)
approximate = False
no_dimensions = 3
nu_init = 1e-8
detector_type = 0
tolerance = 1e-9
maximum_iterations = 50
gmres_restart = maximum_iterations

# Initialization of the computed system constants
no_cells = N**3  # Total number of cells
delta_x = L/N  # Cell length
cell_size = delta_x**3  # Cell surface area
no_directions = Sh.get_number_directions(no_dimensions, s_type)
phisz = (1+no_directions)*no_cells
direction_vectors = Sh.initialize_directions(no_dimensions, s_type)
input_object = Gen.KktCalculations(N, L, s_type, alpha, absorption_xsec, scatter_xsec)
absorption_xsec = Sh.Grid3D(absorption_xsec*np.ones(no_cells))  # Place the input x-section onto a grid
scatter_xsec = Sh.Grid3D(scatter_xsec*np.ones(no_cells))  # Place the input x-section onto a grid
src = Sh.Grid3D.initialize_center(N, N/2)  # Source is a square in the center
src = Sh.Grid3D.initialize_dummy_16(N)
sigma_d = Sh.Grid3D(Sh.Grid3D.initialize_border(N).get_repeated_vector(no_directions)) # Detector is integrated on border
Hconstant = 1/(4*np.pi*cell_size*alpha)
index = Gen.get_plot_number()
callback_index = 0
#sigma_d = Sh.Flux.initialize_collimated_detector(N, 2, s_type)
residuals = []



#######################################################################################################################
# Determine the forward transport matrix and calculate the flux from it
a, phi_real = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, src, no_dimensions)
# Calculate the detected flux/current
detected_values = Sh.Flux3D(np.multiply(sigma_d.get_vector(), phi_real.get_vector()), s_type)
# Construct the KKT-matrix and -vector
kkt_mat, kkt_vec, system_size = Sol.create_kkt_system(3, s_type, cell_size, alpha, a, no_cells, 
                                                        sigma_d.get_vector(), detected_values.get_vector())
print('The system has a total size of ' + str(system_size) + ' cells')


#Gen.write_matrix_to_textfile(kkt_mat.toarray(), 'kktmat') #Gen.write_matrix_to_textfile(kkt_vec, 'kktvec')
print('The elapsed time is: ' + str(time.process_time()))
q_sol = Sh.Grid3D(spla.spsolve(kkt_mat, kkt_vec)[:no_cells])
Gen.print_3tensor(q_sol.get_matrix(), N)
#solution_linalg = Sh.KktVector(no_cells, cell_size, s_type, spla.spsolve(kkt_mat, kkt_vec), no_dimensions)
print('The elapsed time is: ' + str(time.process_time()))

print('The elapsed time is: ' + str(time.process_time()))

print('\n')
