# Fill L transport matrix

import numpy as np
import math
from dof_ordering import dof,get_elem_no
from scipy.sparse import csr_matrix

def build_Ltransport(ni,nj,no_ordinates,omegas,weights,dx,dy,Sigma_t,Sigma_s):
  vol = dx * dy
  no_elem = ni * nj
  L_no_dof = no_ordinates * no_elem
  nnz_max = no_ordinates * (5 * no_elem + no_ordinates * no_elem)
  L_row_indices = np.zeros(nnz_max,dtype=int)
  L_col_indices = np.zeros(nnz_max,dtype=int)
  L_data = np.zeros(nnz_max)
  pointer = 0

  for ordinate in range(no_ordinates):
#    print ('')
#    print ('ordinate',ordinate)
    omega = omegas[ordinate]
    for j in range(nj):
      for i in range(ni):
        row = dof(ordinate,i,j,ni,nj)
#        print ('row',row)

        # total removal

        val = weights[ordinate] * vol * Sigma_t[i,j]

        col = row
        L_row_indices[pointer] = row
        L_col_indices[pointer] = col
        L_data[pointer] = val
        pointer = pointer + 1

        # scatter

        for ordinate2 in range(no_ordinates):
          val = - weights[ordinate] * vol * weights[ordinate2] * Sigma_s[i,j] / (4 * math.pi)
          col = dof(ordinate2,i,j,ni,nj)
          L_row_indices[pointer] = row
          L_col_indices[pointer] = col
          L_data[pointer] = val
          pointer = pointer + 1

        # right face

        omega_dot_n = + omega[0]
        val = weights[ordinate] * dy * omega[0]
        if omega_dot_n > 0:
          col = row
#          print ('R col',col)
          L_row_indices[pointer] = row
          L_col_indices[pointer] = col
          L_data[pointer] = val
          pointer = pointer + 1
        else:
          if i < ni-1:
            col = dof(ordinate,i+1,j,ni,nj)
#            print ('R col',col)
            L_row_indices[pointer] = row
            L_col_indices[pointer] = col
            L_data[pointer] = val
            pointer = pointer + 1

        # left face

        omega_dot_n = - omega[0]
        val = weights[ordinate] * dy * omega_dot_n
        if omega_dot_n > 0:
          col = row
#          print ('L col',col)
          L_row_indices[pointer] = row
          L_col_indices[pointer] = col
          L_data[pointer] = val
          pointer = pointer + 1
        else:
          if i > 0:
            col = dof(ordinate,i-1,j,ni,nj)
#            print ('L col',col)
            L_row_indices[pointer] = row
            L_col_indices[pointer] = col
            L_data[pointer] = val
            pointer = pointer + 1

        # top face

        omega_dot_n = + omega[1]
        val = weights[ordinate] * dx * omega_dot_n
        if omega_dot_n > 0:
          col = row
#          print ('T col',col)
          L_row_indices[pointer] = row
          L_col_indices[pointer] = col
          L_data[pointer] = val
          pointer = pointer + 1
        else:
          if j < nj-1:
            col = dof(ordinate,i,j+1,ni,nj)
#            print ('T col',col)
            L_row_indices[pointer] = row
            L_col_indices[pointer] = col
            L_data[pointer] = val
            pointer = pointer + 1

        # bottom face

        omega_dot_n = - omega[1]
        val = weights[ordinate] * dx * omega_dot_n
        if omega_dot_n > 0:
          col = row
#          print ('B col',col)
          L_row_indices[pointer] = row
          L_col_indices[pointer] = col
          L_data[pointer] = val
          pointer = pointer + 1
        else:
          if j > 0:
            col = dof(ordinate,i,j-1,ni,nj)
#            print ('B col',col)
            L_row_indices[pointer] = row
            L_col_indices[pointer] = col
            L_data[pointer] = val
            pointer = pointer + 1

  # put in matrix

  L = csr_matrix((L_data,(L_row_indices,L_col_indices)),shape=(L_no_dof,L_no_dof))

  return L
