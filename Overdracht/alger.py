import numpy as np
import General_Functions as Gen
import Transport2D as Tr
import KKT_solver as Sol
import KKT_shape as Sh
import Minres
import time
from scipy.sparse import linalg as spla
from scipy import sparse
from math import sqrt
#########################################################################################################################
def cost_function(q, phi):
    difference = sigma_d.dot(phi) - Dphi
    J = 0.5*delta_x*np.dot(difference, difference) + .5*4*np.pi*cell_size*alpha*np.dot(q, q)
    return J
    
def minimum_check():
    scaling = 1e-3
    cost = cost_function(solution_linalg.get_q(orientation='vec'), solution_linalg.get_phi().get_vector())
    test_q = Sh.Grid2D(solution_linalg.get_q(orientation='vec'))
    test_phi = solution_linalg.get_phi().get_vector()
    print('Cost of solution: ', cost)
    for i in range(no_cells):
        pert_q = solution_linalg.get_q(orientation='vec').copy()
        pert_q[i] += scaling
        pert_q = Sh.Grid2D(pert_q)
        pert_phi = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, pert_q, print_output=False)[1]
        pert_cost = cost_function(pert_q.get_vector(), pert_phi.get_vector())
        cost_difference =  pert_cost - cost
        print('Perturbation: ', i, ' - cost difference: ', cost_difference)
        if cost_difference < 0:
            print('NOOOOOOOOOOOOOOOOOOO')
        pert_q = solution_linalg.get_q(orientation='vec').copy()
        pert_q[i] -= scaling
        pert_q = Sh.Grid2D(pert_q)
        pert_phi = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, pert_q, print_output=False)[1]
        pert_cost = cost_function(pert_q.get_vector(), pert_phi.get_vector())
        cost_difference =  pert_cost - cost
        print('Perturbation: ', i, ' - cost difference: ', cost_difference)
        if cost_difference < 0:
            print('NOOOOOOOOOOOOOOOOOOO')

# Preconditioning methods
def no_preco(vec):
    return vec


def c_product(vec):
    y = np.zeros(no_directions*no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(1+k)*no_cells] = -weight*np.pi*cell_size*vec
    return y


def ct_product(vec):
    y = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y -= weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    return y 
        

def cct_product(vec):
    temp = np.zeros(no_cells)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        temp += weight*np.pi*cell_size*vec[k*no_cells:(k+1)*no_cells]
    y = np.zeros(no_cells*no_directions)
    for k in range(no_directions):
        weight = direction_vectors[k].get_weight()
        y[k*no_cells:(k+1)*no_cells] += weight*np.pi*cell_size*temp
    return y
        


def callback(vec):
    global callback_index
    print('Iteration number ' + str(callback_index) + ' \t Residual ' + '{:.2e}'.format(vec))
    residuals.append(vec)
    callback_index += 1


def newton():
    nu = nu_init
    x = np.ones(system_size)
    x[:no_cells] = solution_linalg.get_vector()[:no_cells]
    for i in range(no_cells):
        if x[i] <= 0:
            x[i] = 1e-15
    converged = False
    i = 0
    while not converged:
        #sk = mv_product(x)
        sk = kkt_mat.dot(x)
        q_k = x[:no_cells]
        sk[:no_cells] -= nu/q_k
        sk[no_cells:phisz] -= detected_values
        newt_mat = kkt_mat.copy()
        newt_mat[:no_cells, :no_cells] += sparse.diags(nu/np.multiply(q_k, q_k), 0, format='lil')
        delta_xk = spla.spsolve(newt_mat, -sk)
        s = 1
        step_size_converged = False
        while not step_size_converged:
            if min(x + s*delta_xk) <= 0:
                s *= 0.9
            else:
                step_size_converged = True
        x += s*delta_xk
        relative_newton_norm = np.linalg.norm(delta_xk)/np.linalg.norm(x)
        print('Newton step ', i, '\t nu = ', '{:.2e}'.format(nu), '\t relative norm = ' + '{:.2e}'.format(relative_newton_norm), '\t s = ' + '{:.2e}'.format(s))
        if relative_newton_norm  <= 1e-3 or s <= 1e-4:
            converged = True
            print('Newton converged on step  ', i, '\t with relative norm ', relative_newton_norm)
        nu *= 0.5
        if i % 3 == 0 or converged:
            write_x = Sh.KktVector(no_cells, cell_size, s_type, delta_x*x, 2)
            input_object.write_source_to_file(write_x, 'normal' + str(i), index)
        i += 1
        if i >= 90:
            print('Newton did not converge after 90 steps.')
            break
    return x, converged


def alger(vec):
    rho = sqrt(alpha)
    h = 1/(4*np.pi*cell_size*alpha) 
    for k in range(no_directions):
        h += rho*(np.pi*cell_size*direction_vectors[k].get_weight())**2
    p11 = h * sparse.identity(no_cells, format='lil') #+ rho*CT.dot(C)
    #p11 = 4*np.pi*cell_size*alpha * sparse.identity(no_cells, format='lil') + rho*CT.dot(C)
    Ksqrt = sparse.lil_matrix((no_cells*no_directions, no_cells*no_directions))
    for i in range(no_cells*no_directions):
        Ksqrt[i, i] = sqrt(K[i, i])
    #p22 = (sqrt(rho)*adj + Ksqrt).dot(sqrt(rho)*a + Ksqrt) - sqrt(rho)*adj.dot(Ksqrt) - sqrt(rho)*Ksqrt.dot(a)
    p22 = K + rho*a.transpose().dot(a)
    p33 = rho * sparse.identity(no_cells*no_directions, format='lil')
    result = np.zeros(system_size)
    #result[:no_cells] = spla.spsolve(p11, vec[:no_cells])
    result[:no_cells] = p11.dot(vec[:no_cells])  # Solve for q
    result[no_cells:phisz] = spla.spsolve(p22, vec[no_cells:phisz])  # Solve for phi
    #result[no_cells:phisz] = p22(vec[no_cells:phisz])
    result[phisz:] = p33.dot(vec[phisz:])  # Solve for phi^T
    return result


########################################################################################################################
# Initialization of user input constants
N = 16  # Number of cells in one x-direction
L = 1  # Length of the system
alpha = 1E-6  # Regularization constant
absorption_xsec = 0.048  #.048 cm^-1 - For 140 keV gammas
scatter_xsec = 0.172  #.172 cm^-1 - For 140 keV gammas
s_type = 4   # Measure for the amount of direction discretizations. Currently choose between S2 (1) and S4 (3)
preconditioner_type = alger
approximate = False
no_dimensions = 2
nu_init = 1e-8
detector_type = 0
tolerance = 1e-6
maximum_iterations = 500
gmres_restart = maximum_iterations

# Initialization of the computed system constants
no_cells = N**2  # Total number of cells
delta_x = L/N  # Cell length
cell_size = delta_x**2  # Cell surface area
no_directions = Sh.get_number_directions(no_dimensions, s_type)
phisz = (1+no_directions)*no_cells
direction_vectors = Sh.initialize_directions(no_dimensions, s_type)
input_object = Gen.KktCalculations(N, L, s_type, alpha, absorption_xsec, scatter_xsec)
absorption_xsec = Sh.Grid2D(absorption_xsec*np.ones(N**2))  # Place the input x-section onto a grid
scatter_xsec = Sh.Grid2D(scatter_xsec*np.ones(N**2))  # Place the input x-section onto a grid
src = Sh.Grid2D.initialize_center(N)  # Source is a square in the center
sigma_d = Sh.Flux2D.initialize_collimated_detector_scheef(N, no_dimensions, s_type)
Hconstant = 1/(4*np.pi*cell_size*alpha)
#sigma_d = Sh.Flux2D.initialize_current_detector(N, no_dimensions, s_type)
K = delta_x*sigma_d.transpose().dot(sigma_d)
residuals = []
callback_index = 0


#######################################################################################################################
# Determine the forward transport matrix and calculate the flux from it
a, phi_real = Tr.calculate_phi(N, delta_x, cell_size, s_type, absorption_xsec, scatter_xsec, src)
#rl_det = Sh.Flux2D.initialize_collimated_detector_fout(N, no_dimensions, s_type)
#detected_values = Sh.Flux2D(K.dot(phi_real.get_vector()), s_type)
Dphi = sigma_d.dot(phi_real.get_vector())
detected_values = K.dot(phi_real.get_vector())
#detected_values2 = Sh.Flux2D(rl_det.dot(phi_real.get_vector()), s_type)

# Calculate the detected flux/current
"""print(detected_values.get_vector())
with open('sigmadtest.txt', 'w') as f:
    Gen.write_matrix(sigma_d.toarray(), f, approximate=False) 
"""
# Construct the KKT-matrix and -vector
kkt_mat, kkt_vec, system_size = Sol.create_kkt_system(2, s_type, delta_x, alpha, a, N, K, detected_values)
print('The system has a total size of ' + str(system_size) + ' cells')


#######################################################################################################################
# Produce the linear solution for comparison
index = Gen.get_plot_number()

#Gen.write_matrix_to_textfile(kkt_mat.toarray(), 'kktmat') #Gen.write_matrix_to_textfile(kkt_vec, 'kktvec')
print('The elapsed time is: ' + str(time.process_time()))
#solution_linalg = Sh.KktVector(no_cells, cell_size, s_type, spla.spsolve(kkt_mat, kkt_vec), 2)
#solution_linalg2 = Sh.KktVector(no_cells, cell_size, s_type, spla.spsolve(kkt_mat2, kkt_vec2), 2)

#######################################################################################################################
# Iterative system

preco = spla.LinearOperator((system_size, system_size), preconditioner_type)
solution_krylov = spla.gmres(kkt_mat, kkt_vec, tol=tolerance, restart=gmres_restart, maxiter=maximum_iterations, M=preco, callback=callback)[0]
write_x = Sh.KktVector(no_cells, cell_size, s_type, solution_krylov, 2)
input_object.write_source_to_file(write_x, 'alger', index)

print('The elapsed time is: ' + str(time.process_time()))

print('Calculating direct solution')
solution_linalg = Sh.KktVector(no_cells, cell_size, s_type, spla.spsolve(kkt_mat, kkt_vec), 2)
input_object.write_source_to_file(solution_linalg, 'linalg', index)

print('End of program')
print('The elapsed time is: ' + str(time.process_time()))
exit()
#minimum_check()
solution_newton, converged = newton()
print('Newton solution converged = ', converged)
print('The elapsed time is: ' + str(time.process_time()))
#######################################################################################################################

#solution_minres, errors = Minres.solve_minres_preconditioned(kkt_mat, kkt_vec, no_cells, s_type, a, adj, alpha, cell_size, sigma_d.get_vector(), preconditioner_type=preconditioner_type, approximate=approximate, accuracy=1e-6, issparse=issparse)
print('\n')
