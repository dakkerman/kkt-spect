import numpy as np


def calculate_phi(n, delta_x, absorption_xsec, scatter_xsec, source):
    sys_matrix = np.zeros((2 * n, 2* n))
    right_vector = np.zeros(2*n)
    for i in range(n):
        # Calculate the incoming upwind factor
        if i > 0:
            sys_matrix[i, i - 1] += -(1 / delta_x)
        if i < n-1:
            sys_matrix[i + n, i + n + 1] += -(1 / delta_x)
        # Calculate the outgoing upwind factor
        sys_matrix[i, i] += 1 / delta_x
        sys_matrix[i + n, i + n] += 1 / delta_x
        # Calculate inscatter contribution
        sys_matrix[i, i] += -scatter_xsec[i] / 2
        sys_matrix[i + n, i] += -scatter_xsec[i + n] / 2
        sys_matrix[i + n, i + n] += -scatter_xsec[i + n] / 2
        sys_matrix[i, i + n] += -scatter_xsec[i] / 2
        # Calculate outscatter and absorption
        sys_matrix[i, i] += scatter_xsec[i] + absorption_xsec[i]
        sys_matrix[i + n, i + n] += scatter_xsec[i + n] + absorption_xsec[i + n]
        # Calculate source contribution
        right_vector = source.get_source()
    phis = np.linalg.solve(sys_matrix, right_vector)
    return phis[0:n], phis[n:(2*n)], sys_matrix


def calculate_adjoint(n, delta_x, absorption_xsec, scatter_xsec, detector):
    adj_matrix = np.zeros((2 * n, 2 * n))
    adjoint_right_vector = np.zeros(2*n)

    for i in range(n):
        # Calculate the incoming upwind factor
        """if i > 0:
            adjoint_matrix[i+1, i] += (1/delta_x)"""
        if i < n - 1:
            adj_matrix[i, i + 1] += -1 / delta_x
        """        if i < N_cells-1:
            adjoint_matrix[i+N_cells, i+N_cells+1] += -(1/delta_x)"""
        if i > 0:
            adj_matrix[i + n, i + n - 1] += -1 / delta_x
        # Calculate the outgoing upwind factor
        adj_matrix[i, i] += 1 / delta_x
        adj_matrix[i + n, i + n] += 1 / delta_x
        # Calculate inscatter contribution
        adj_matrix[i, i] += -scatter_xsec[i] / 2
        adj_matrix[i + n, i] += -scatter_xsec[i + n] / 2
        adj_matrix[i + n, i + n] += -scatter_xsec[i + n] / 2
        adj_matrix[i, i + n] += -scatter_xsec[i] / 2
        # Calculate outscatter and absorption
        adj_matrix[i, i] += scatter_xsec[i] + absorption_xsec[i]
        adj_matrix[i + n, i + n] += scatter_xsec[i + n] + absorption_xsec[i + n]
        # Calculate source contribution
        adjoint_right_vector = detector.get_source()
    phi_t = np.linalg.solve(adj_matrix, adjoint_right_vector)
    return phi_t[0:n], phi_t[n:(2*n)], adj_matrix


