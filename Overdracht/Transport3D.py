import numpy as np 
import KKT_shape as Sh
from math import sqrt
from scipy import sparse
# import General_Functions as Gen


def calculate_phi(n, delta_x, cell_size, s_type: int, sigma_a: Sh.Grid2D, sigma_s: Sh.Grid2D, src: Sh.Grid2D, no_dim: int):
    print("Calculating transport matrix.")
    face_size = delta_x**2
    no_cells = n**no_dim  # The amount of cells in a 2-dimensional grid
    directions = Sh.get_number_directions(no_dim, s_type)  # The number of direction vectors that are discretized
    size = no_cells*directions  # The size that mat needs to be to contain all cells and all directions
    mat = sparse.lil_matrix((size, size))  # Initialize the matrix
    vec = np.zeros(size)  # Initialize the right-hand side vector
    direction_vectors = Sh.initialize_directions(no_dim, s_type)
    normal_vectors = Sh.initialize_normals(no_dim)
    ###
    if no_dim > 1:
        y_iterations = n
    else:
        y_iterations = 1
    if no_dim > 2:
        z_iterations = n
    else:
        z_iterations = 1
    for k in range(directions):  # Iterate over all directions
        weight = .5*direction_vectors[k].get_weight()
        for y in range(n):  # Iterate over all columns
            for x in range(y_iterations):  # Iterate over all rows
                for z in range(z_iterations):  # Iterate over all third dimension rows
                    index = Sh.get_vector_index(n, x, j=y, direction=k, l=z, no_dim=no_dim)  # The index of this cell and direction in the vector notation
                    space_index = Sh.get_vector_index(n, x, j=y, direction=0, l=z, no_dim=no_dim)  # Index for all isotropic quantities (cross sections
                    ###
                    # Calculate the source term
                    vec[index] = weight*np.pi*cell_size*src.get(x, y, z)
                    ###
                    # Flow into and from the cell
                    for p in range(6):
                        flow = weight*np.pi*face_size*np.dot(normal_vectors[p].get_normalized(),
                                                    direction_vectors[k].get_normalized())
                        if flow > 0:
                            mat[index, index] += flow  # For ths normal vector, the flow is incoming
                        else:
                            if normal_vectors[p].get_vector()[0] != 0:
                                neighbour = normal_vectors[p].get_vector()[0]
                                if 0 <= x + neighbour < n:  # Check if it is a border cell
                                    mat[index, index + neighbour] += flow  # Incoming flow (row)
                            elif normal_vectors[p].get_vector()[1] != 0:
                                neighbour = normal_vectors[p].get_vector()[1]
                                if 0 <= y + neighbour < n:  # Check if it is a border cell
                                    mat[index, index + neighbour*n] += flow  # Incoming flow (row)
                            elif normal_vectors[p].get_vector()[2] != 0:
                                neighbour = normal_vectors[p].get_vector()[2]
                                if 0 <= z + neighbour < n:
                                    mat[index, index + neighbour*(n**2)] += flow
                            else:
                                raise ValueError
                    ###
                    # Calculate scatter and absorption
                    mat[index, index] += weight*np.pi*cell_size*sigma_s.get(x, y, z)  # Out scatter
                    mat[index, index] += weight*np.pi*cell_size*sigma_a.get(x, y, z)  # Absorption
                    for p in range(directions):  # Calculate in scatter from all four directions
                        scatter_weight = .5*direction_vectors[p].get_weight()
                        #mat[index, space_index + p*no_cells] -= 2*scatter_weight*weight*np.pi*cell_size*sigma_s.get(x,y,z)/directions 
                        mat[index, space_index + p*no_cells] -= weight*np.pi*cell_size*sigma_s.get(x,y,z)/directions  # The line above SHOULD work, but this one DOES work....

    print('Solving the transport matrix to obtain the flux')
    from scipy.sparse import linalg as spalg
    mat = mat.tocsr()
    raw_phi = spalg.spsolve(mat, vec)
    phi = Sh.Flux3D(raw_phi, s_type)
    ###
    # Check whether the balance between production, absorption and leakage holds
    if not calculate_balance(delta_x, cell_size, phi, src, sigma_a):
        raise ValueError('The balance does not hold')
    return mat, phi


def calculate_adjoint(n, delta_x, cell_size, s_type, sigma_a: Sh.Grid2D, sigma_s: Sh.Grid2D):
    print('Calculating adjoint')
    no_cells = n**2
    directions = Sh.get_number_directions(2, s_type)
    size = no_cells * directions
    mat = np.zeros((size, size))
    flow_factor = np.pi * delta_x / sqrt(3)
    for k in range(directions):
        direction_vector = Sh.direction_vector(k)
        for j in range(n):
            for i in range(n):
                index = Sh.get_vector_index(n, i, j, k, no_dim=no_dim)
                space_index = Sh.get_vector_index(n, i, j, no_dim=no_dim)
                # Flow
                mat[index, index] = 2*flow_factor
                if 0 <= i + direction_vector[0] < n-1:
                    mat[index, index + direction_vector[0]] -= flow_factor
                if 0 <= j + direction_vector[1] < n:
                    mat[index, index + direction_vector[1] * n] -= flow_factor
                # Calculate out scatter and absorption
                mat[index, index] += np.pi*cell_size*sigma_s.get(i, j) + np.pi*cell_size*sigma_a.get(i, j)
                # Calculate in scatter from all four directions
                for p in range(directions):
                    mat[index, space_index + p*no_cells] -= np.pi*cell_size*sigma_s.get(i, j)/directions
    return mat


def calculate_balance(delta_x, cell_size, phi: Sh.Flux3D, src: Sh.Grid2D, sx: Sh.Grid2D):
    print('Checking if the balance of production, absorption and leakage approximates zero')
    src_sum = src.calculate_total_value(cell_size)
    print('Total production is    ' + str(src_sum))
    phi_out = phi.calculate_outgoing_flux(delta_x)
    print('Total outgoing flux is ' + str(phi_out))
    absorption = np.multiply(phi.scalar_flux(False), sx.get_matrix())
    absorption = Sh.Grid3D.initialize_from_matrix(absorption)
    absorption_sum = absorption.calculate_total_value(cell_size, integrated_over_all_directions=False)
    print('Total absorption is    ' + str(absorption_sum))
    balance = src_sum - phi_out - absorption_sum
    return -1e-6 < balance < 1e-6
